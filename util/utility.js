module.exports.enhanceMasterVariantData = ( data ) => {
    let enhancedctpproducts = data.ctproducts;
    if(!data.ctproducts.masterVariant && data.ctproducts.masterData.current.masterVariant){
      enhancedctpproducts['masterVariant']= data.ctproducts.masterData.current.masterVariant;
      data['ctproducts'] = enhancedctpproducts;
    }
  }