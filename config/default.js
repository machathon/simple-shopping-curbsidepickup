module.exports = exports = {
 port: 4000,
// Uniform downloads the intent data at build time and bakes it into your site so there are no //additional required network requests.
 UNIFORM_API_KEY: '<UNIFORM_KEY>',
 // Contentstack Config
 contentstack: {
 api_key: '<API_KEY>',
 access_token: '<ACCESS_TOKEN>',
 ct_extension_id:'<CS_CT_EXTENSION_ID>',
 },
 commercetools: {
 'project_id': '<CT_PROJECT_ID>',
 'host': '<CT_API_HOST_DOMAIN>', // api host url
 'client_id': '<CLIENT_ID>',
 'client_secret': '<CLIENT_SECRET>',
 'scopes': ['<PROJECT_SCOPE>']
 },
 algolia:{
 'index_name': '<ALGOLIA_INDEX>',
 'application_id': '<ALGOLIA_APP_ID>',
 'search_api_key': '<ALGOLIA_SEARCH_API_KEY>'
 }
};