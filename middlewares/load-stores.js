const async = require('async');
const {getStores} = require('../lib/commercetool');
const util = require('util');

module.exports = function(req, res, next) {
  async.parallel([
    function(callback) {

      let storesDataResults = [];
      getStores().then((storesData) =>{
        storesData.body.results.forEach(function(data){
          storesDataResults.push({ id: data.id, name: data.name.en, key:data.key });
        })
        if(storesDataResults.length!==0){
          callback(null, {'list':storesDataResults});
        }else{
          callback({'Error':'Failed with an error'});
        }
      });
    },
  ], function(error, success) {
    if (error) return next(error);
    console.log('load-stores success >>>', success[0]);
    res.locals.stores = success[0];
    next();
  });
};
