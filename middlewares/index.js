const express = require('express');
const router = express.Router();
router.get('*',require('./load-partials'));
router.get('*',require('./load-stores'));
module.exports = router;
