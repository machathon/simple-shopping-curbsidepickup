const {createClient} = require('@commercetools/sdk-client');
const {createAuthMiddlewareForClientCredentialsFlow} = require('@commercetools/sdk-middleware-auth');
const {createHttpMiddleware} = require('@commercetools/sdk-middleware-http');
const fetch = require('node-fetch');
const projectKey = config.commercetools.project_id;
const util = require('util')


const authMiddleware = createAuthMiddlewareForClientCredentialsFlow({
  host: `https://auth.${config.commercetools.host}`,
  projectKey,
  credentials: {
    clientId: config.commercetools.client_id,
    clientSecret: config.commercetools.client_secret,
  },
  scopes: config.scopes,
  fetch,
});
const httpMiddleware = createHttpMiddleware({
  host: `https://api.${config.commercetools.host}`,
  fetch,
});
const client = createClient({
  middlewares: [authMiddleware, httpMiddleware],
});

module.exports.getAllProducts =async ()=>{
  try {
    const productBody={
      uri: `/${projectKey}/products`,
      method: 'GET',
    };
	// alternative shortcut
   console.log(util.inspect(productBody, false, null, true /* enable colors */));
    return await client.execute(productBody);
  } catch (err) {
    console.log('Error fetching products information. Error'+err);
    res.send({err})
  }
};

module.exports.createCart= async (req, res, next)=>{
  try {
    const cartBody={
      currency: 'USD',
      customerEmail: 'dummy@gmail.com',
      lineItems: req.body,
      shippingAddress:{        
        country:'US'
      }
    };
	
    const orderBody={
      uri: `/${projectKey}/in-store/key=${req.params.storeKey}/carts`,
      method: 'POST',
      body: JSON.stringify(cartBody),
    };
	console.log(util.inspect(orderBody, false, null, true /* enable colors */));
    const cartResponse=await client.execute(orderBody);
    if (cartResponse.body.id) {
      res.send({status: 200, cartId: cartResponse.body.id});
    }
  } catch (err) {
    console.log('Failed to create Cart. Error:'+err);
    res.send({err})
  }
};

module.exports.createOrderFromCart= async (req, res)=>{
  try {
    let ordernumber = "O"+Math.floor(100000 + (Math.random() * 900000));
    console.log("order:"+ordernumber);
    const orderCreateBody ={
      uri: `/${projectKey}/in-store/key=${req.body.storeId}/orders`,
      method: 'POST',
      body: JSON.stringify({id: req.body.id, version: 1, orderNumber: ordernumber}),
    };
    const orderResponse= await client.execute(orderCreateBody);
    if (orderResponse.body.id) {
      res.send({status: 200, orderId: orderResponse.body.id,orderNumber: ordernumber});
    }
  } catch (err) {
    console.log('Failed to create Order. Error'+err);
    res.send({err})
  }
};

module.exports.getCartDetails =async (req, res)=>{
  try {
    const getCartBody={
      uri: `/${projectKey}/carts/${req.params.cartId}`,
      method: 'GET',
    };
    const cart= await client.execute(getCartBody);
    res.render('pages/payment', {
      totalPrice: cart.body.totalPrice.centAmount/100,
      cartId: cart.body.id,
    });
  } catch (err) {
    console.log('Failed to get Cart Details. Error'+err);
    res.send({err})
  }  
};

module.exports.getStores =async ()=>{
  try {
    const getStoresBody={
      uri: `/${projectKey}/stores`,
      method: 'GET',
    };
    return await client.execute(getStoresBody);   
  } catch (err) {
    console.log('Failed to get stores response. Error'+err);
    return {error: 'Failed to fetch results'}
  }
};