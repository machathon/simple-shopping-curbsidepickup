# simple-shopping-curbsidepickup

## Create an E-commerce Website Using Contentstack , commercetools, Algolia, Live Chat, Voucherify .

**About Contentstack**: [Contentstack](https://www.contentstack.com/) is a headless CMS with an API-first approach that puts content at the centre. It is designed to simplify the process of publication by separating code from content.

**About Commercetools**: [Commercetools](https://www.commercetools.com/) is a highly scalable cloud platform with a flexible commerce API at its core, which supports a modern microservice-based architecture and offers a wide range of integrations.

**About Algolia**: [Algolia](https://www.algolia.com) is a flexible AI-powered Search & Discovery platform

**About Live chat**: [Live Chat](https://www.livechat.com) is an online customer service software with online chat, help desk software, and web analytics capabilities.

**About Uniform**: [Uniform](https://uniform.dev/) is an online personalistaion platform which makes the fastest personalized sites possible at global scale, with predictable, controllable costs, using your existing DXP. Products.

### Live Demo
You can check the [live demo](https://ielectra-machathon.click/) to get the first-hand experience of the website 

