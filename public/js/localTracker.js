import {
  IntentTagStrength,
  SignalType
} from "./web_modules/@uniformdev/optimize-common.js";
import {createDefaultTracker} from "./web_modules/@uniformdev/optimize-tracker-browser.js";

import intentManifest from "./intentManifest.json.proxy.js";

const finalIntentManifest = {
  ...intentManifest,
  site: {
    ...intentManifest.site,
    intents: [
      ...intentManifest.site.intents,
      {
        id: "extra-event-intent",
        signals: [
          {
            type: SignalType.Event,
            str: IntentTagStrength.Boost,
            label: {
              expr: "/category/watches",
              type: "exact"
            }
          }
        ]
      }
    ]
  }
};
export const localTracker = createDefaultTracker({
  intentManifest: finalIntentManifest
});
