/** Standard presets for the strength of an intent tag. Note that values of any integer are valid; these are references to the default strengths. */
var IntentTagStrength;
(function (IntentTagStrength) {
    IntentTagStrength[IntentTagStrength["Antimatter"] = 0] = "Antimatter";
    IntentTagStrength[IntentTagStrength["Tangential"] = 5] = "Tangential";
    IntentTagStrength[IntentTagStrength["Weak"] = 25] = "Weak";
    IntentTagStrength[IntentTagStrength["Normal"] = 50] = "Normal";
    IntentTagStrength[IntentTagStrength["Strong"] = 75] = "Strong";
    IntentTagStrength[IntentTagStrength["Strongest"] = 100] = "Strongest";
    IntentTagStrength[IntentTagStrength["Boost"] = 1000] = "Boost";
})(IntentTagStrength || (IntentTagStrength = {}));
/** Extracts personalization-enabled intents from an IntentTags record */
function getPersonalizationIntents(tags) {
    if (!(tags === null || tags === void 0 ? void 0 : tags.intents)) {
        return {};
    }
    return Object.keys(tags === null || tags === void 0 ? void 0 : tags.intents).reduce(function (result, intentId) {
        var vector = tags.intents[intentId];
        if (!vector.noPn) {
            result[intentId] = vector;
        }
        return result;
    }, {});
}

export { IntentTagStrength as I, getPersonalizationIntents as g };
