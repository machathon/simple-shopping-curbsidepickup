var SignalType;
(function (SignalType) {
    SignalType["Cookie"] = "CK";
    SignalType["Event"] = "EVT";
    SignalType["Behavior"] = "BEH";
    SignalType["LandingPage"] = "LP";
    SignalType["PageViewCount"] = "PVW";
    SignalType["PageVisited"] = "PVI";
    SignalType["QueryString"] = "QS";
})(SignalType || (SignalType = {}));

export { SignalType as S };
