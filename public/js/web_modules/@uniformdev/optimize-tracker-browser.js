import { I as IntentTagStrength } from '../common/IntentTags-dc424c30.js';
import { S as SignalType } from '../common/Signal-8d5664ea.js';

function isCookieSignal(signal) {
    return signal.type == SignalType.Cookie;
}

function isEventSignal(signal) {
    return signal.type == SignalType.Event;
}

function isBehaviorSignal(signal) {
    return signal.type == SignalType.Behavior;
}

function isLandingPageSignal(signal) {
    return signal.type == SignalType.LandingPage;
}

function isPageViewCountSignal(signal) {
    return signal.type == SignalType.PageViewCount;
}

function isPageVisitedSignal(signal) {
    return signal.type == SignalType.PageVisited;
}

function isQueryStringSignal(signal) {
    return signal.type == SignalType.QueryString;
}

// long term, want more types i.e. 'regex', 'substring', 'wildcard'
// these types may contain additional props i.e. indexes for substring, or options like /i for regex
/** Tests if a StringMatch matches a string value */
function isStringMatch(value, match) {
    var _a;
    var _b, _c, _d, _e;
    var localValue = value;
    var localExpr = match === null || match === void 0 ? void 0 : match.expr;
    if (!((_b = match === null || match === void 0 ? void 0 : match.cs) !== null && _b !== void 0 ? _b : false)) {
        _a = CaseDesensitize(localValue, localExpr), localValue = _a[0], localExpr = _a[1];
    }
    switch ((_c = match === null || match === void 0 ? void 0 : match.type) !== null && _c !== void 0 ? _c : 'exact') {
        case 'exact':
            return localValue === localExpr;
        case 'notexact':
            return localValue !== localExpr;
        case 'contains':
            if (!localExpr) {
                return false;
            }
            return (_d = localValue === null || localValue === void 0 ? void 0 : localValue.includes(localExpr)) !== null && _d !== void 0 ? _d : false;
        case 'notcontains':
            if (!localExpr) {
                return true;
            }
            return !((_e = localValue === null || localValue === void 0 ? void 0 : localValue.includes(localExpr)) !== null && _e !== void 0 ? _e : true);
        case 'exists':
            return value !== null && typeof value !== 'undefined';
        case 'notexists':
            return value === null || typeof value === 'undefined';
        default:
            console.warn("Unknown string match type " + match.type + " will be ignored.");
            return false;
    }
}
function CaseDesensitize() {
    var exprs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        exprs[_i] = arguments[_i];
    }
    return exprs.map(function (e) { return e === null || e === void 0 ? void 0 : e.toUpperCase(); });
}

var Scope;
(function (Scope) {
    Scope["Visit"] = "visit";
    Scope["Visitor"] = "visitor";
})(Scope || (Scope = {}));

var SignalFrequency;
(function (SignalFrequency) {
    SignalFrequency["OncePerVisit"] = "visit";
    SignalFrequency["Once"] = "once";
    SignalFrequency["Always"] = "always";
})(SignalFrequency || (SignalFrequency = {}));

var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var defaultConfig = {
    maxDecay: 0.95,
    daysInMonth: 30,
};
var calculateDifference = function (first, second) {
    var dayDiff = Math.round((first - second) / (1000 * 60 * 60 * 24));
    return dayDiff;
};
var calculateDecayRemainder = function (days, daysInMonth, maxDecay) {
    if (!daysInMonth) {
        console.warn('[DecayByDay]: daysInMonth is not populated.');
        return null;
    }
    var decay;
    if (maxDecay) {
        decay = Math.min(maxDecay, days / daysInMonth);
    }
    else {
        decay = days / daysInMonth;
    }
    var remainder = 1 - decay;
    return remainder;
};
var decayByDayStrategy = function (parameters) {
    var options = __assign(__assign({}, defaultConfig), parameters);
    var decayByDay = function (trackerScoring) {
        if (!(trackerScoring === null || trackerScoring === void 0 ? void 0 : trackerScoring.updated) || !trackerScoring.values) {
            return Promise.resolve(undefined);
        }
        var updated = trackerScoring.updated, values = trackerScoring.values;
        var today = options.today || new Date().valueOf();
        var dayDiff = calculateDifference(today, updated !== null && updated !== void 0 ? updated : 0);
        var remainderAfterDecay = calculateDecayRemainder(dayDiff, options.daysInMonth, options.maxDecay);
        var response = __assign({ values: {}, updated: 0, strategy: '?' }, trackerScoring);
        // no decay factor so we apply nothing
        if (remainderAfterDecay === 1) {
            return Promise.resolve(undefined);
        }
        Object.keys(values).forEach(function (score) {
            var _a, _b;
            var currentValueReference = values[score];
            if (typeof currentValueReference === 'number') {
                currentValueReference = values[score] = { str: currentValueReference };
            }
            var currentValue = Number((_b = (_a = values[score]) === null || _a === void 0 ? void 0 : _a.str) !== null && _b !== void 0 ? _b : 0);
            if (remainderAfterDecay) {
                response.values[score].str = currentValue * remainderAfterDecay;
            }
        });
        return Promise.resolve(response);
    };
    return decayByDay;
};

var __assign$1 = (undefined && undefined.__assign) || function () {
    __assign$1 = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign$1.apply(this, arguments);
};
var cumulativeScoringStrategy = function () {
    var identifier = 'cumulative';
    var strategy = function (_a) {
        var previousValue = _a.previousValue, valueToAdd = _a.valueToAdd, signalCount = _a.signalCount;
        var response = __assign$1({}, previousValue);
        var signalNames = Object.keys(valueToAdd);
        signalNames.forEach(function (signal) {
            var _a, _b, _c;
            var newValue = (_a = valueToAdd[signal]) === null || _a === void 0 ? void 0 : _a.str;
            if (newValue === IntentTagStrength.Antimatter) {
                // antimatter (strength 0) removes ALL intent strength
                delete response[signal];
            }
            else {
                var weightedValue = Number(newValue !== null && newValue !== void 0 ? newValue : 0) / signalCount;
                response[signal] = { str: (Number((_c = (_b = response[signal]) === null || _b === void 0 ? void 0 : _b.str) !== null && _c !== void 0 ? _c : 0) || 0) + weightedValue };
            }
        });
        return {
            values: response,
            strategy: identifier,
            updated: new Date().valueOf(),
        };
    };
    return strategy;
};

var __assign$2 = (undefined && undefined.__assign) || function () {
    __assign$2 = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign$2.apply(this, arguments);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var OptimizeTracker = /** @class */ (function () {
    function OptimizeTracker(options) {
        var _a;
        var _b, _c;
        this._intentMappings = [];
        this._behaviorValues = [];
        this._events = [];
        this._listeners = [];
        this._testSettings = [];
        this._signalEvaluators = {};
        this._serverStateSignalMatchesProcessed = false;
        this._initialized = false;
        this._previouslyBroadcastedScore = undefined;
        this._plugins = options.plugins;
        this._storage = options.storage;
        this._scopeStrategies = options.scopes;
        this._scoringStrategy = (_b = options.scoring) !== null && _b !== void 0 ? _b : cumulativeScoringStrategy();
        this._decayStrategy = (_c = options.decay) !== null && _c !== void 0 ? _c : decayByDayStrategy();
        this._serverState = this.getServerState();
        if (options.testing) {
            var resolvedTesting = !Array.isArray(options.testing) ? [options.testing] : options.testing;
            (_a = this._testSettings).push.apply(_a, resolvedTesting);
        }
    }
    OptimizeTracker.prototype.isIncludedInTest = function (options) {
        var _a, _b;
        var _c = options || {}, scope = _c.scope, sampleSize = _c.sampleSize;
        var now = new Date();
        var activeTestSettings = this._testSettings.find(function (setting) { return setting.strategy && (!setting.endDate || setting.endDate > now); });
        if (!activeTestSettings) {
            return false;
        }
        var scopeToResolve = scope !== null && scope !== void 0 ? scope : Scope.Visitor;
        var resolvedScope = (_b = (_a = this._scopeStrategies) === null || _a === void 0 ? void 0 : _a[scopeToResolve]) === null || _b === void 0 ? void 0 : _b.get();
        var isInTest = activeTestSettings.strategy.isIncludedInTest(resolvedScope, sampleSize);
        return isInTest;
    };
    OptimizeTracker.prototype.getScopeNames = function (scope) {
        if (!this._scopeStrategies) {
            return [];
        }
        if (scope && this._scopeStrategies[scope]) {
            return [scope];
        }
        var names = Object.keys(this._scopeStrategies);
        return names;
    };
    OptimizeTracker.prototype.removeIntent = function (intent, scope) {
        return __awaiter(this, void 0, void 0, function () {
            var scopes, wasSomethingRemovedFromAnyScope, _loop_1, this_1, _a, _b, _i, currentScope;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        scopes = this.getScopeNames(scope);
                        wasSomethingRemovedFromAnyScope = false;
                        _loop_1 = function (currentScope) {
                            var scoring, updatedScoring_1, somethingWasRemoved_1;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this_1._storage.scoring.getScoring(currentScope)];
                                    case 1:
                                        scoring = _a.sent();
                                        if (scoring) {
                                            updatedScoring_1 = {};
                                            somethingWasRemoved_1 = false;
                                            Object.keys(scoring.values).map(function (key) {
                                                if (intent && key !== intent) {
                                                    updatedScoring_1[key] = scoring.values[key];
                                                }
                                                else {
                                                    somethingWasRemoved_1 = true;
                                                    wasSomethingRemovedFromAnyScope = true;
                                                }
                                            });
                                            if (somethingWasRemoved_1) {
                                                this_1.writeScoring(currentScope, __assign$2(__assign$2({}, scoring), { values: updatedScoring_1 }));
                                            }
                                        }
                                        return [2 /*return*/];
                                }
                            });
                        };
                        this_1 = this;
                        _a = [];
                        for (_b in scopes)
                            _a.push(_b);
                        _i = 0;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        currentScope = _a[_i];
                        return [5 /*yield**/, _loop_1(currentScope)];
                    case 2:
                        _c.sent();
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, wasSomethingRemovedFromAnyScope];
                }
            });
        });
    };
    OptimizeTracker.prototype.getIntentStrength = function (scope) {
        return __awaiter(this, void 0, void 0, function () {
            var scopes, scoring, _i, scopes_1, currentScope, _a, _b, combined;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        scopes = this.getScopeNames(scope);
                        scoring = {};
                        _i = 0, scopes_1 = scopes;
                        _c.label = 1;
                    case 1:
                        if (!(_i < scopes_1.length)) return [3 /*break*/, 4];
                        currentScope = scopes_1[_i];
                        _a = scoring;
                        _b = currentScope;
                        return [4 /*yield*/, this._storage.scoring.getScoring(currentScope)];
                    case 2:
                        _a[_b] = _c.sent();
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4:
                        combined = this.combineScoring(scoring);
                        return [2 /*return*/, combined];
                }
            });
        });
    };
    OptimizeTracker.prototype.getIntentStrengthSingle = function (vector, scope) {
        return __awaiter(this, void 0, void 0, function () {
            var scoring;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getIntentStrength(scope)];
                    case 1:
                        scoring = _a.sent();
                        return [2 /*return*/, scoring === null || scoring === void 0 ? void 0 : scoring[vector]];
                }
            });
        });
    };
    OptimizeTracker.prototype.getServerIntentScores = function () {
        var _a;
        return (_a = this._serverState) === null || _a === void 0 ? void 0 : _a.scoring;
    };
    OptimizeTracker.prototype.getRequestContext = function () {
        if (typeof window === 'undefined') {
            return undefined;
        }
        return {
            cookies: document.cookie,
            url: window.location.href,
            userAgent: navigator.userAgent,
        };
    };
    OptimizeTracker.prototype.log = function (data) {
        this._plugins.log(function (log) { return log(data); });
    };
    OptimizeTracker.prototype.trackPersonalization = function (name, payload) {
        if (name) {
            this._plugins.track(function (track) { return track(name, payload); });
        }
    };
    OptimizeTracker.prototype.getServerState = function () {
        if (typeof document === 'undefined') {
            return;
        }
        var matchesElement = document.getElementById('__UNIFORM_DATA__');
        if (!matchesElement) {
            return;
        }
        var parsed = JSON.parse(matchesElement.textContent);
        return parsed;
    };
    OptimizeTracker.prototype.resolveScope = function (scope) {
        var _a, _b;
        var loggingRegion = 'Resolve Scope';
        var scopeToResolve = scope || Scope.Visitor;
        var resolvedScope = (_b = (_a = this._scopeStrategies) === null || _a === void 0 ? void 0 : _a[scopeToResolve]) === null || _b === void 0 ? void 0 : _b.get();
        if (!resolvedScope) {
            this.log({
                level: 'warn',
                message: "Scope \"" + scope + "\" returned null or undefined",
                region: loggingRegion,
            });
        }
        if ((resolvedScope === null || resolvedScope === void 0 ? void 0 : resolvedScope.type) !== scope) {
            this.log({
                level: 'warn',
                message: "Scope \"" + scope + "\" was requested, but \"" + (resolvedScope === null || resolvedScope === void 0 ? void 0 : resolvedScope.type) + "\" was resolved",
                region: loggingRegion,
            });
        }
        return resolvedScope;
    };
    OptimizeTracker.prototype.resolveSignalStorage = function (scope, signal) {
        var _a, _b;
        var loggingRegion = 'Resolve Signal Storage';
        var signalStorage = signal.id ? (_a = scope === null || scope === void 0 ? void 0 : scope.state.signals) === null || _a === void 0 ? void 0 : _a[signal.id] : {};
        if (signal.id && !((_b = scope === null || scope === void 0 ? void 0 : scope.state.signals) === null || _b === void 0 ? void 0 : _b[signal.id])) {
            this.log({
                level: 'verbose',
                message: "No state found for signal \"" + signal.id + "\", using default state",
                region: loggingRegion,
            });
        }
        else {
            this.log(__assign$2({ level: 'verbose', message: "State found for signal \"" + signal.id + "\"", region: loggingRegion }, signalStorage));
        }
        return signalStorage;
    };
    OptimizeTracker.prototype.resolveFrequencyScope = function (signal) {
        var statisticsScopeName = signal.freq === SignalFrequency.OncePerVisit ? Scope.Visit : Scope.Visitor;
        var statisticsScope = this.resolveScope(statisticsScopeName);
        return statisticsScope;
    };
    OptimizeTracker.prototype.determineIfSignalShouldEvaluate = function (signal) {
        var _a, _b;
        if (!signal.id) {
            this.log({
                level: 'info',
                message: "Signal ID is not populated",
            });
            return false;
        }
        if (!signal.freq || signal.freq === SignalFrequency.Always) {
            return true;
        }
        var statisticsScope = this.resolveFrequencyScope(signal);
        return !((_b = (_a = statisticsScope === null || statisticsScope === void 0 ? void 0 : statisticsScope.statistics) === null || _a === void 0 ? void 0 : _a[signal.id]) === null || _b === void 0 ? void 0 : _b.evaluations);
    };
    OptimizeTracker.prototype.reevaluateSignal = function (_a) {
        var _b;
        var intent = _a.intent, signal = _a.signal, requestContext = _a.requestContext;
        return __awaiter(this, void 0, void 0, function () {
            var loggingRegion, shouldSignalEvaluate, signalEvaluator, scope, signalStorage, evaluationResult, evaluatorOptions, signalResult, result, statisticsScope, statistics;
            var _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        loggingRegion = 'Evaluate Signal';
                        shouldSignalEvaluate = this.determineIfSignalShouldEvaluate(signal);
                        if (!shouldSignalEvaluate) {
                            this.log({
                                level: 'info',
                                message: "Signal " + signal.id + " will not evaluate",
                                region: loggingRegion,
                            });
                            return [2 /*return*/, undefined];
                        }
                        signalEvaluator = this._signalEvaluators[signal.type];
                        if (!signalEvaluator) {
                            this.log({
                                level: 'warn',
                                message: "Signal " + signal.type + " not handled by any plugin. It will be ignored.",
                                region: loggingRegion,
                            });
                            return [2 /*return*/, undefined];
                        }
                        if (!signal.str && signal.str !== IntentTagStrength.Antimatter) {
                            this.log({
                                level: 'warn',
                                message: "Strength not defined on signal, setting strength to " + IntentTagStrength.Normal,
                                region: loggingRegion,
                            });
                            // default strength
                            signal.str = IntentTagStrength.Normal;
                        }
                        scope = this.resolveScope(signal.scope);
                        signalStorage = this.resolveSignalStorage(scope, signal);
                        evaluationResult = {
                            intent: intent,
                            signal: signal,
                            scope: scope,
                        };
                        evaluatorOptions = {
                            signal: signal,
                            intent: intent,
                            behaviors: this._behaviorValues,
                            state: signalStorage,
                            requestContext: requestContext || this.getRequestContext(),
                            events: this._events,
                            log: this.log.bind(this),
                        };
                        this.log(__assign$2({ level: 'verbose', message: "Signal evaluator options", region: loggingRegion }, evaluatorOptions));
                        signalResult = signalEvaluator(evaluatorOptions);
                        return [4 /*yield*/, Promise.resolve(signalResult)];
                    case 1:
                        result = _d.sent();
                        this.log({
                            level: 'verbose',
                            message: "Signal evaluation result",
                            region: loggingRegion,
                            signal: signal,
                            intent: intent,
                            result: result,
                        });
                        evaluationResult.result = result;
                        if (typeof result === 'undefined') {
                            return [2 /*return*/, evaluationResult];
                        }
                        statisticsScope = this.resolveFrequencyScope(signal);
                        if (statisticsScope && signal.id) {
                            statistics = ((_b = statisticsScope.statistics) === null || _b === void 0 ? void 0 : _b[signal.id]) || {
                                evaluations: 0,
                            };
                            statisticsScope.statistics = __assign$2(__assign$2({}, (statisticsScope.statistics || {})), (_c = {}, _c[signal.id] = {
                                evaluations: ++statistics.evaluations,
                            }, _c));
                        }
                        if (scope && signal.id && result.state) {
                            scope.state.signals = scope.state.signals || {};
                            scope.state.signals[signal.id] = result.state;
                        }
                        return [2 /*return*/, evaluationResult];
                }
            });
        });
    };
    OptimizeTracker.prototype.processSignalEvaluations = function (evaluationResults) {
        var summary = {};
        evaluationResults.forEach(function (evaluationResult) {
            var _a;
            var signal = evaluationResult.signal, scope = evaluationResult.scope, result = evaluationResult.result, intent = evaluationResult.intent;
            var id = intent.id;
            if (scope && result && (result.strength || result.strength === IntentTagStrength.Antimatter)) {
                var _b = summary[scope.type] || {
                    matches: [],
                    score: {},
                }, score = _b.score, matches = _b.matches;
                var str = (score[id] || {
                    str: 0,
                }).str;
                summary[scope.type] = {
                    score: (_a = {},
                        _a[id] = {
                            str: Number(str) + Number(result.strength),
                        },
                        _a),
                    matches: __spreadArrays([
                        {
                            intentId: id,
                            signalStrength: result.strength,
                            signalType: signal.type,
                        }
                    ], matches),
                };
            }
        });
        this.log({
            level: 'verbose',
            message: "Signal evaluation matches",
            region: 'Process Signal Evaluations',
            summary: summary,
        });
        return summary;
    };
    OptimizeTracker.prototype.reevaluateIntent = function (_a) {
        var intent = _a.intent, requestContext = _a.requestContext;
        return __awaiter(this, void 0, void 0, function () {
            var loggingRegion, results, id, signals, _i, _b, signal, evaluationResult;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        loggingRegion = 'Reevaluate Intent';
                        results = [];
                        id = intent.id, signals = intent.signals;
                        if (!(!signals || signals.length === 0)) return [3 /*break*/, 1];
                        this.log({
                            level: 'warn',
                            message: "Intent " + id + " does not define any signals.",
                            region: loggingRegion,
                        });
                        return [3 /*break*/, 5];
                    case 1:
                        _i = 0, _b = intent.signals;
                        _c.label = 2;
                    case 2:
                        if (!(_i < _b.length)) return [3 /*break*/, 5];
                        signal = _b[_i];
                        return [4 /*yield*/, this.reevaluateSignal({ intent: intent, signal: signal, requestContext: requestContext })];
                    case 3:
                        evaluationResult = _c.sent();
                        if (evaluationResult === null || evaluationResult === void 0 ? void 0 : evaluationResult.result) {
                            results.push(evaluationResult);
                        }
                        _c.label = 4;
                    case 4:
                        _i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/, results];
                }
            });
        });
    };
    OptimizeTracker.prototype.clearBehaviorQueue = function () {
        if (this._behaviorValues.length) {
            var cleared = this._behaviorValues.splice(0, this._behaviorValues.length);
            this.log(__assign$2({ level: 'verbose', message: "Clearing behavior queue", region: 'Clear Behavior Queue' }, cleared));
        }
    };
    OptimizeTracker.prototype.clearEventQueue = function () {
        if (this._events.length) {
            var cleared = this._events.splice(0, this._events.length);
            this.log(__assign$2({ level: 'verbose', message: "Clearing event queue", region: 'Clear Event Queue' }, cleared));
        }
    };
    OptimizeTracker.prototype.processServerSideMatches = function (summary) {
        var _this = this;
        var _a, _b;
        var loggingRegion = 'Process SSR Matches';
        var allSsrMatches = !this._serverStateSignalMatchesProcessed || !((_a = this._serverState) === null || _a === void 0 ? void 0 : _a.matches)
            ? {}
            : (_b = this._serverState) === null || _b === void 0 ? void 0 : _b.matches;
        var clientMatches = {};
        if (allSsrMatches) {
            this.log(__assign$2({ level: 'verbose', message: "Discovered SSR matches", region: loggingRegion }, allSsrMatches));
        }
        this.log({
            level: 'verbose',
            message: "Processing SSR matches",
            region: loggingRegion,
        });
        Object.keys(summary).forEach(function (scope) {
            var matches = summary[scope].matches;
            var ssrMatches = allSsrMatches[scope];
            if (matches && matches.length) {
                matches.forEach(function (match) {
                    var found = ssrMatches === null || ssrMatches === void 0 ? void 0 : ssrMatches.find(function (ssrMatch) { return _this.doSignalsMatch(ssrMatch, match); });
                    _this.log(__assign$2({ level: 'verbose', message: "SSR match processed", region: loggingRegion }, found));
                    if (!found) {
                        clientMatches[scope] = clientMatches[scope] || [];
                        clientMatches[scope].push(match);
                    }
                    else {
                        _this.log(__assign$2({ level: 'info', message: "Ignoring client side match" }, match));
                    }
                });
            }
            _this.log({
                level: 'verbose',
                message: "SSR proccessing complete",
                region: loggingRegion,
            });
            _this._serverStateSignalMatchesProcessed = true;
        });
        return clientMatches;
    };
    OptimizeTracker.prototype.applySummary = function (summary) {
        return __awaiter(this, void 0, void 0, function () {
            var loggingRegion, scopeNames, scopeScoring, _i, scopeNames_1, currentScope, _a, score, matches, previousScore, _b, _c, combinedScoring;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        loggingRegion = 'Write Summary To Storage';
                        scopeNames = this.getScopeNames();
                        scopeScoring = {};
                        _i = 0, scopeNames_1 = scopeNames;
                        _d.label = 1;
                    case 1:
                        if (!(_i < scopeNames_1.length)) return [3 /*break*/, 6];
                        currentScope = scopeNames_1[_i];
                        _a = summary[currentScope] || {}, score = _a.score, matches = _a.matches;
                        return [4 /*yield*/, this.readScoring(currentScope)];
                    case 2:
                        previousScore = _d.sent();
                        if (!(score && matches)) return [3 /*break*/, 4];
                        _b = scopeScoring;
                        _c = currentScope;
                        return [4 /*yield*/, this.applyScoring(currentScope, previousScore, score, matches.length)];
                    case 3:
                        _b[_c] = _d.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        scopeScoring[currentScope] = previousScore;
                        _d.label = 5;
                    case 5:
                        _i++;
                        return [3 /*break*/, 1];
                    case 6:
                        this.log(__assign$2({ level: 'verbose', message: "Scoring by scope", region: loggingRegion }, scopeScoring));
                        combinedScoring = this.combineScoring(scopeScoring);
                        this.log(__assign$2({ level: 'verbose', message: "Combined scope scoring", region: loggingRegion }, combinedScoring));
                        return [2 /*return*/, combinedScoring];
                }
            });
        });
    };
    OptimizeTracker.prototype.reevaluateSignals = function (requestContext) {
        return __awaiter(this, void 0, void 0, function () {
            var loggingRegion, signalResults, _i, _a, intent, resolvedItems, summary, combinedScoring, signalMatches;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        loggingRegion = 'Reevaluate Signals';
                        this.log({ level: 'verbose', message: 'Signals reevaluating...', region: loggingRegion });
                        if (!this._intentMappings || !this._intentMappings.length) {
                            this.log({ level: 'warn', message: 'Intents list is empty', region: loggingRegion });
                            return [2 /*return*/, {
                                    signalMatches: {},
                                }];
                        }
                        signalResults = [];
                        _i = 0, _a = this._intentMappings;
                        _b.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        intent = _a[_i];
                        return [4 /*yield*/, this.reevaluateIntent({
                                intent: intent,
                                requestContext: requestContext,
                            })];
                    case 2:
                        resolvedItems = _b.sent();
                        signalResults.push.apply(signalResults, resolvedItems);
                        _b.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4:
                        summary = this.processSignalEvaluations(signalResults);
                        return [4 /*yield*/, this.applySummary(summary)];
                    case 5:
                        combinedScoring = _b.sent();
                        return [4 /*yield*/, this.broadcastScoring(combinedScoring)];
                    case 6:
                        _b.sent();
                        this.clearBehaviorQueue();
                        this.clearEventQueue();
                        signalMatches = this.processServerSideMatches(summary);
                        this.log({
                            level: 'info',
                            message: "Reevaluated signals",
                            region: loggingRegion,
                            scoring: combinedScoring,
                            signalMatches: signalMatches,
                        });
                        return [2 /*return*/, {
                                scoring: combinedScoring,
                                signalMatches: signalMatches,
                            }];
                }
            });
        });
    };
    OptimizeTracker.prototype.combineScoring = function (scopeScoring) {
        var response = {};
        if (scopeScoring) {
            Object.keys(scopeScoring).forEach(function (currentScope) {
                var scoring = scopeScoring[currentScope];
                if (scoring && scoring.values) {
                    Object.keys(scoring.values).forEach(function (key) {
                        if (!response[key]) {
                            response[key] = { str: 0 };
                        }
                        var value = scoring.values[key];
                        if (value.str) {
                            response[key].str = Number(response[key].str) + Number(value.str);
                        }
                    });
                }
            });
        }
        return response;
    };
    OptimizeTracker.prototype.doSignalsMatch = function (ssrMatch, match) {
        return (match.intentId === ssrMatch.intentId &&
            match.signalType === ssrMatch.signalType &&
            match.signalStrength === ssrMatch.signalStrength);
    };
    OptimizeTracker.prototype.addBehaviorActivity = function (behaviorIntentVector) {
        this.log(__assign$2({ level: 'verbose', message: 'Behavior activity added' }, behaviorIntentVector));
        this._behaviorValues.push(behaviorIntentVector);
    };
    OptimizeTracker.prototype.initialize = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.initializeScopes()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.loadPlugins()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.loadIntents()];
                    case 3:
                        _a.sent();
                        this._signalEvaluators = this.loadSignalEvaluators();
                        this._initialized = true;
                        return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.isInitialized = function () {
        return this._initialized;
    };
    OptimizeTracker.prototype.addScoringChangeListener = function (listener) {
        this._listeners.push(listener);
    };
    OptimizeTracker.prototype.removeScoringChangeListener = function (listener) {
        var position = this._listeners.indexOf(listener);
        if (position > -1) {
            this._listeners.splice(position, 1);
        }
    };
    OptimizeTracker.prototype.loadPlugins = function () {
        return __awaiter(this, void 0, void 0, function () {
            var plugins, i, plugin;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this._plugins) {
                            return [2 /*return*/];
                        }
                        plugins = this._plugins.findPlugins('initialize');
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < plugins.length)) return [3 /*break*/, 4];
                        plugin = plugins[i];
                        return [4 /*yield*/, plugin.initialize({
                                log: this.log.bind(this),
                            })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.loadSignalEvaluators = function () {
        var _this = this;
        var signals = this._plugins.find('signalEvaluatorMapping');
        return signals.reduce(function (result, plugin) {
            var pluginEvaluatorMappings = plugin();
            Object.keys(pluginEvaluatorMappings).forEach(function (mappedSignalType) {
                if (result[mappedSignalType]) {
                    _this.log({
                        level: 'warn',
                        message: "[Tracker]: Multiple plugins provided signal resolvers for " + mappedSignalType,
                    });
                }
                result[mappedSignalType] = pluginEvaluatorMappings[mappedSignalType];
            });
            return result;
        }, {});
    };
    OptimizeTracker.prototype.loadIntents = function () {
        return __awaiter(this, void 0, void 0, function () {
            var intents;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._plugins.loadIntents()];
                    case 1:
                        intents = _a.sent();
                        this.addIntents(intents);
                        return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.addIntents = function (intents) {
        var _a;
        this.log({ level: 'info', message: 'Received intents', intents: intents });
        var processedIntents = intents.map(function (intent) {
            var signals = intent.signals;
            return __assign$2(__assign$2({}, intent), { signals: signals.map(function (signal) {
                    var id = signal.id;
                    return __assign$2(__assign$2({}, signal), { id: id !== null && id !== void 0 ? id : intent.id + ":" + JSON.stringify(signal) });
                }) });
        });
        (_a = this._intentMappings).push.apply(_a, processedIntents);
    };
    OptimizeTracker.prototype.readScoring = function (scope) {
        return __awaiter(this, void 0, void 0, function () {
            var scoring, preDecay, decayedScoring;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._storage.scoring.getScoring(scope)];
                    case 1:
                        scoring = _a.sent();
                        if (!(scoring && this._decayStrategy)) return [3 /*break*/, 3];
                        preDecay = __assign$2({}, scoring);
                        return [4 /*yield*/, this._decayStrategy(scoring)];
                    case 2:
                        decayedScoring = _a.sent();
                        if (typeof decayedScoring !== 'undefined') {
                            scoring = decayedScoring;
                        }
                        if (scoring) {
                            this.log({
                                level: 'verbose',
                                message: 'Decay has been calculated',
                                pre: preDecay,
                                post: scoring,
                            });
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/, scoring];
                }
            });
        });
    };
    OptimizeTracker.prototype.gatherScopeStrategies = function () {
        var discovered = [];
        if (this._scopeStrategies) {
            var scopes = Object.keys(this._scopeStrategies);
            for (var _i = 0, scopes_2 = scopes; _i < scopes_2.length; _i++) {
                var key = scopes_2[_i];
                var strategy = this._scopeStrategies[key];
                if (strategy) {
                    discovered.push(strategy);
                }
            }
        }
        return discovered;
    };
    OptimizeTracker.prototype.initializeScopes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var strategies, _i, strategies_1, strategy;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        strategies = this.gatherScopeStrategies();
                        _i = 0, strategies_1 = strategies;
                        _a.label = 1;
                    case 1:
                        if (!(_i < strategies_1.length)) return [3 /*break*/, 4];
                        strategy = strategies_1[_i];
                        return [4 /*yield*/, strategy.initialize({
                                log: this.log.bind(this),
                            })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.saveScopes = function () {
        return __awaiter(this, void 0, void 0, function () {
            var strategies, _i, strategies_2, strategy;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        strategies = this.gatherScopeStrategies();
                        _i = 0, strategies_2 = strategies;
                        _a.label = 1;
                    case 1:
                        if (!(_i < strategies_2.length)) return [3 /*break*/, 4];
                        strategy = strategies_2[_i];
                        return [4 /*yield*/, strategy.save()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.writeToStorage = function (scope, score) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var expirationMinutes;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        expirationMinutes = (_b = (_a = this._scopeStrategies) === null || _a === void 0 ? void 0 : _a[scope]) === null || _b === void 0 ? void 0 : _b.getExpiration();
                        return [4 /*yield*/, this._storage.scoring.setScoring(score, scope, expirationMinutes)];
                    case 1:
                        _c.sent();
                        return [4 /*yield*/, this.saveScopes()];
                    case 2:
                        _c.sent();
                        return [2 /*return*/, score];
                }
            });
        });
    };
    OptimizeTracker.prototype.addIntentStrength = function (scope, valueToAdd) {
        return __awaiter(this, void 0, void 0, function () {
            var currentValue;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.readScoring(scope)];
                    case 1:
                        currentValue = _a.sent();
                        return [4 /*yield*/, this.applyScoring(scope, currentValue, valueToAdd, 1)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.broadcastScoring()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.applyScoring = function (scope, previousValue, valueToAdd, signalCount) {
        var scoring = this._scoringStrategy({
            previousValue: (previousValue === null || previousValue === void 0 ? void 0 : previousValue.values) || {},
            valueToAdd: valueToAdd,
            signalCount: signalCount,
        });
        return this.writeScoring(scope, scoring);
    };
    OptimizeTracker.prototype.writeScoring = function (scope, scoring) {
        this._plugins.scoringChange(function (func) { return func(scoring === null || scoring === void 0 ? void 0 : scoring.values); });
        this.log(__assign$2({ level: 'verbose', message: 'Scoring has been updated' }, scoring.values));
        return this.writeToStorage(scope, scoring); // yolo. swag. git push -f origin master
    };
    OptimizeTracker.prototype.broadcastScoring = function (scoring) {
        return __awaiter(this, void 0, void 0, function () {
            var value, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = scoring;
                        if (_a) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.getIntentStrength()];
                    case 1:
                        _a = (_b.sent());
                        _b.label = 2;
                    case 2:
                        value = _a;
                        this._previouslyBroadcastedScore = scoring;
                        this._listeners.forEach(function (listener) { return listener(value || null); });
                        return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.forgetMe = function () {
        return __awaiter(this, void 0, void 0, function () {
            var scopes, _i, scopes_3, currentScope;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._storage.scopes.delete()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this._storage.activities.delete()];
                    case 2:
                        _a.sent();
                        scopes = this.getScopeNames();
                        _i = 0, scopes_3 = scopes;
                        _a.label = 3;
                    case 3:
                        if (!(_i < scopes_3.length)) return [3 /*break*/, 6];
                        currentScope = scopes_3[_i];
                        return [4 /*yield*/, this._storage.scoring.delete(currentScope)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5:
                        _i++;
                        return [3 /*break*/, 3];
                    case 6:
                        this.broadcastScoring();
                        return [2 /*return*/];
                }
            });
        });
    };
    OptimizeTracker.prototype.addEvent = function (event, options) {
        return __awaiter(this, void 0, void 0, function () {
            var quiet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this._events.push(event);
                        quiet = options && options.quiet !== undefined && options.quiet;
                        if (!!quiet) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.reevaluateSignals()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    return OptimizeTracker;
}());

var addLocalIntentManifestPlugin = function (intentManifest) {
    return {
        name: 'intents-local',
        onLoadIntents: function () { var _a, _b; return Promise.resolve((_b = (_a = intentManifest === null || intentManifest === void 0 ? void 0 : intentManifest.site) === null || _a === void 0 ? void 0 : _a.intents) !== null && _b !== void 0 ? _b : []); },
    };
};

var logLevelMap = {
    error: 3,
    warn: 2,
    info: 1,
    verbose: 0,
};
var isLogLevelUnderThreshold = function (threshold, level) {
    if (logLevelMap[threshold] !== undefined &&
        logLevelMap[level] !== undefined &&
        logLevelMap[level] < logLevelMap[threshold]) {
        return false;
    }
    return true;
};

var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var addConsoleLoggerPlugin = function (logLevelThreshold) {
    if (logLevelThreshold === void 0) { logLevelThreshold = 'info'; }
    return {
        name: 'console-logger',
        onLogMessage: function (_a) {
            var _b = _a.level, level = _b === void 0 ? 'verbose' : _b, message = _a.message, state = __rest(_a, ["level", "message"]);
            var shouldOutput = isLogLevelUnderThreshold(logLevelThreshold, level);
            if (!shouldOutput) {
                return;
            }
            var style = 'color: black';
            // eslint-disable-next-line no-console
            var func = console.log;
            if (level === 'error') {
                func = console.error;
                // browser already styles errors
                style = '';
            }
            else if (level === 'warn') {
                func = console.warn;
                // browser already styles warnings
                style = '';
            }
            else if (level === 'verbose') {
                style = 'color: gray; font-size: 0.9em;';
            }
            var consoleMessage = "%c[Uniform] " + message;
            if (state) {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                var region = state.region, finalState = __rest(state, ["region"]);
                if (region) {
                    consoleMessage = "%c[Uniform] [" + region + "] " + message;
                }
                var hasOtherKeys = Object.keys(finalState).length > 0;
                if (hasOtherKeys) {
                    func(consoleMessage, style, finalState);
                }
                else {
                    func(consoleMessage, style);
                }
            }
            else {
                func(consoleMessage, style);
            }
        },
    };
};

var __spreadArrays$1 = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var behaviorSignalEvaluator = function (_a) {
    var signal = _a.signal, behaviors = _a.behaviors, intent = _a.intent, log = _a.log;
    var loggingRegion = 'Evaluate Signal: Behavior';
    if (!isBehaviorSignal(signal)) {
        log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Signal is not a behavior signal', region: loggingRegion });
        return undefined;
    }
    var id = intent.id;
    var behaviorSignalStrength = signal.str;
    var resultStrength = 0;
    log === null || log === void 0 ? void 0 : log({
        level: 'verbose',
        message: 'Processing behaviors for intent',
        behaviors: __spreadArrays$1(behaviors),
        intentId: id,
    });
    behaviors.forEach(function (evt) {
        var _a;
        var tagStrength = (_a = evt[id]) === null || _a === void 0 ? void 0 : _a.str;
        if (tagStrength && behaviorSignalStrength) {
            var tagStrengthScaled = Number(tagStrength) / behaviors.length;
            if (tagStrength !== tagStrengthScaled) {
                log === null || log === void 0 ? void 0 : log({
                    level: 'verbose',
                    message: 'Scaling strength',
                    prev: tagStrength,
                    after: tagStrengthScaled,
                    region: loggingRegion,
                });
            }
            var behScaled = Number(behaviorSignalStrength) / 100;
            var tagScaled = tagStrengthScaled / 100;
            var scaledStrength = behScaled * tagScaled;
            var total = scaledStrength * 100;
            log === null || log === void 0 ? void 0 : log({
                level: 'verbose',
                message: 'Total strength',
                region: loggingRegion,
                beh: behScaled,
                tag: tagScaled,
                total: total,
            });
            resultStrength += total;
        }
    });
    if (resultStrength === 0) {
        log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: "No pending behavior matched \"" + id + "\"", region: loggingRegion });
        return undefined;
    }
    log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Signal strength', region: loggingRegion, resultStrength: resultStrength });
    return {
        strength: resultStrength,
    };
};
var addBehaviorSignalPlugin = function () { return ({
    name: SignalType.Behavior + "-signal",
    signalEvaluatorMapping: function () {
        var _a;
        return (_a = {}, _a[SignalType.Behavior] = behaviorSignalEvaluator, _a);
    },
}); };

var getCookie = function (name, context) {
    if (!context || !context.cookies) {
        return undefined;
    }
    var value = "; " + context.cookies;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) {
        return parts.pop().split(';').shift();
    }
};
var cookieSignalEvaluator = function (_a) {
    var signal = _a.signal, requestContext = _a.requestContext, log = _a.log;
    var loggingRegion = 'Evaluate Signal: Cookie';
    if (!isCookieSignal(signal)) {
        log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Signal is not a cookie signal', region: loggingRegion });
        return undefined;
    }
    var cookieValue = getCookie(signal.parameter, requestContext);
    if (!cookieValue) {
        log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Cookie value is not defined', region: loggingRegion });
        return undefined;
    }
    log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Received cookie value', cookieValue: cookieValue, region: loggingRegion });
    var isMatch = isStringMatch(cookieValue, signal.value);
    if (!isMatch) {
        log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Cookie value does not match signal', region: loggingRegion });
        return undefined;
    }
    log === null || log === void 0 ? void 0 : log({
        level: 'verbose',
        message: 'Tracking cookie',
        name: signal.parameter,
        strength: signal.str,
        region: loggingRegion,
    });
    var result = {
        strength: Number(signal.str),
    };
    return result;
};
var addCookieSignalPlugin = function () { return ({
    name: SignalType.Cookie + "-signal",
    signalEvaluatorMapping: function () {
        var _a;
        return (_a = {}, _a[SignalType.Cookie] = cookieSignalEvaluator, _a);
    },
}); };

var doesValueMatchEventProperty = function (stringValue, stringMatch) {
    if (!stringMatch) {
        return true;
    }
    if (!stringValue) {
        return false;
    }
    var doesLabelMatch = isStringMatch(stringValue, stringMatch);
    return doesLabelMatch;
};
var eventSignalEvaluator = function (_a) {
    var signal = _a.signal, events = _a.events, log = _a.log;
    var loggingRegion = 'Evaluate Signal: Event';
    if (!isEventSignal(signal)) {
        log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Signal is not an event signal', region: loggingRegion });
        return undefined;
    }
    if (!events || !events.length) {
        log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'No events to process', region: loggingRegion });
        return undefined;
    }
    var strength = 0;
    var matched = false;
    events.forEach(function (event) {
        var matchesLabel = doesValueMatchEventProperty(event.label, signal.label);
        var matchesCategory = doesValueMatchEventProperty(event.category, signal.category);
        var matchesValue = doesValueMatchEventProperty(event.value, signal.value);
        if (matchesLabel && matchesCategory && matchesValue) {
            log === null || log === void 0 ? void 0 : log({ level: 'info', message: "Event Match", region: loggingRegion, event: event, signal: signal });
            strength += Number(signal.str);
            matched = true;
        }
    });
    if (!matched) {
        log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: "No event matches", region: loggingRegion, signal: signal });
        return undefined;
    }
    return {
        strength: strength,
    };
};
var addEventSignalPlugin = function () { return ({
    name: SignalType.Event + "-signal",
    signalEvaluatorMapping: function () {
        var _a;
        return (_a = {}, _a[SignalType.Event] = eventSignalEvaluator, _a);
    },
}); };

var __awaiter$1 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$1 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var landingPageSignalEvaluator = function (_a) {
    var signal = _a.signal, requestContext = _a.requestContext, log = _a.log;
    return __awaiter$1(void 0, void 0, void 0, function () {
        var loggingRegion, url, strength;
        return __generator$1(this, function (_b) {
            loggingRegion = 'Evaluate Signal: Landing Page';
            if (!isLandingPageSignal(signal)) {
                log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Signal is not an landing page signal', region: loggingRegion });
                return [2 /*return*/, undefined];
            }
            if (!requestContext || !requestContext.url) {
                log === null || log === void 0 ? void 0 : log({
                    level: 'verbose',
                    message: "Request context not defined",
                    region: loggingRegion,
                    requestContext: !!requestContext,
                    url: requestContext === null || requestContext === void 0 ? void 0 : requestContext.url,
                });
                return [2 /*return*/, undefined];
            }
            url = new URL(requestContext.url);
            log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Checking URL', region: loggingRegion, url: url, criteria: signal.path });
            if (isStringMatch(url.pathname, signal.path)) {
                log === null || log === void 0 ? void 0 : log({ level: 'info', message: 'URL is a match', region: loggingRegion, url: url, criteria: signal.path });
                strength = Number(signal.str);
            }
            else {
                log === null || log === void 0 ? void 0 : log({
                    level: 'info',
                    message: 'URL is not a match',
                    region: loggingRegion,
                    url: url,
                    criteria: signal.path,
                });
            }
            return [2 /*return*/, {
                    strength: strength,
                }];
        });
    });
};
var addLandingPageSignalPlugin = function () {
    return {
        name: SignalType.LandingPage + "-signal",
        signalEvaluatorMapping: function () {
            var _a;
            return (_a = {},
                _a[SignalType.LandingPage] = landingPageSignalEvaluator,
                _a);
        },
    };
};

var pageViewCountSignalEvaluator = function (_a) {
    var _b, _c, _d, _e;
    var signal = _a.signal, state = _a.state, log = _a.log;
    var loggingRegion = 'Evaluate Signal: Page View';
    if (!isPageViewCountSignal(signal)) {
        log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Signal is not a page view count signal', region: loggingRegion });
        return undefined;
    }
    var pageCount = (_b = state === null || state === void 0 ? void 0 : state.pageCount) !== null && _b !== void 0 ? _b : 0;
    log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Starting page count', region: loggingRegion, pageCount: pageCount });
    var result = {
        // we need to always update the state whether we matched or not
        state: {
            pageCount: pageCount + 1,
        },
    };
    log === null || log === void 0 ? void 0 : log({
        level: 'verbose',
        message: 'Incrementing page count',
        region: loggingRegion,
        pageCount: (_c = result.state) === null || _c === void 0 ? void 0 : _c.pageCount,
    });
    var min = (_d = signal === null || signal === void 0 ? void 0 : signal.min) !== null && _d !== void 0 ? _d : 0;
    var max = (_e = signal === null || signal === void 0 ? void 0 : signal.max) !== null && _e !== void 0 ? _e : Number.MAX_VALUE;
    log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Checking if page count is in range', region: loggingRegion, min: min, max: max });
    if (pageCount >= min && pageCount <= max) {
        log === null || log === void 0 ? void 0 : log({
            level: 'info',
            message: 'Is in range, setting strength',
            region: loggingRegion,
            str: signal.str,
        });
        result.strength = Number(signal.str);
    }
    else {
        log === null || log === void 0 ? void 0 : log({
            level: 'verbose',
            message: 'Is not in range, ignoring.',
            region: loggingRegion,
            str: signal.str,
        });
    }
    return result;
};
var addPageViewCountSignalPlugin = function () { return ({
    name: SignalType.PageViewCount + "-signal",
    signalEvaluatorMapping: function () {
        var _a;
        return (_a = {}, _a[SignalType.PageViewCount] = pageViewCountSignalEvaluator, _a);
    },
}); };

var pageVisitedSignalEvaluator = function (_a) {
    var signal = _a.signal, requestContext = _a.requestContext, log = _a.log;
    var loggingRegion = 'Evaluate Signal: Page Visited';
    if (!isPageVisitedSignal(signal)) {
        log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Signal is not a page visited signal', region: loggingRegion });
        return undefined;
    }
    if (!requestContext || !requestContext.url) {
        log === null || log === void 0 ? void 0 : log({
            level: 'verbose',
            message: "Request context not defined",
            region: loggingRegion,
            requestContext: !!requestContext,
            url: requestContext === null || requestContext === void 0 ? void 0 : requestContext.url,
        });
        return undefined;
    }
    var url = new URL(requestContext.url);
    log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Checking URL', region: loggingRegion, url: url, criteria: signal.path });
    var strength;
    if (isStringMatch(url.pathname, signal.path)) {
        log === null || log === void 0 ? void 0 : log({ level: 'info', message: 'URL is a match', region: loggingRegion, url: url, criteria: signal.path });
        strength = Number(signal.str);
    }
    else {
        log === null || log === void 0 ? void 0 : log({
            level: 'info',
            message: 'URL is not a match',
            region: loggingRegion,
            url: url,
            criteria: signal.path,
        });
        return undefined;
    }
    return {
        strength: strength,
    };
};
var addPageVisitedSignalPlugin = function () {
    return {
        name: SignalType.PageVisited + "-signal",
        signalEvaluatorMapping: function () {
            var _a;
            return (_a = {}, _a[SignalType.PageVisited] = pageVisitedSignalEvaluator, _a);
        },
    };
};

var queryStringSignalEvaluator = function (_a) {
    var signal = _a.signal, requestContext = _a.requestContext, log = _a.log;
    var loggingRegion = 'Evaluate Signal: Page Visited';
    if (!isQueryStringSignal(signal)) {
        log === null || log === void 0 ? void 0 : log({ level: 'warn', message: 'Signal is not a query string signal', region: loggingRegion });
        return undefined;
    }
    if (!requestContext || !requestContext.url) {
        log === null || log === void 0 ? void 0 : log({
            level: 'verbose',
            message: "Request context not defined",
            region: loggingRegion,
            requestContext: !!requestContext,
            url: requestContext === null || requestContext === void 0 ? void 0 : requestContext.url,
        });
        return undefined;
    }
    var url = new URL(requestContext.url);
    log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Checking URL', region: loggingRegion, url: url });
    var parameterValue = url.searchParams.get(signal.parameter);
    log === null || log === void 0 ? void 0 : log({
        level: 'verbose',
        message: 'Checking URL for query string value',
        region: loggingRegion,
        url: url,
        parameter: signal.parameter,
    });
    if (!parameterValue) {
        log === null || log === void 0 ? void 0 : log({ level: 'verbose', message: 'Query string value not defined', region: loggingRegion, url: url });
        return undefined;
    }
    if (!isStringMatch(parameterValue, signal.value)) {
        log === null || log === void 0 ? void 0 : log({
            level: 'info',
            message: 'Query string value is not a match',
            region: loggingRegion,
            parameterValue: parameterValue,
            criteria: signal.value,
        });
        return undefined;
    }
    var result = {
        strength: Number(signal.str),
    };
    log === null || log === void 0 ? void 0 : log({
        level: 'verbose',
        message: 'Updating tracked queries',
        region: loggingRegion,
    });
    return result;
};
var addQueryStringSignalPlugin = function () { return ({
    name: SignalType.QueryString + "-signal",
    signalEvaluatorMapping: function () {
        var _a;
        return (_a = {},
            _a[SignalType.QueryString] = queryStringSignalEvaluator,
            // legacy
            _a.UTM = queryStringSignalEvaluator,
            _a);
    },
}); };

var __awaiter$2 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$2 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var OptimizePluginLoader = /** @class */ (function () {
    function OptimizePluginLoader(plugins) {
        this._pluginMap = this.mapPlugins(plugins);
    }
    OptimizePluginLoader.prototype.log = function (callback) {
        var functions = this.find('onLogMessage');
        functions.forEach(function (func) { return callback(func); });
    };
    OptimizePluginLoader.prototype.track = function (callback) {
        var functions = this.find('track');
        functions.forEach(function (func) { return callback(func); });
    };
    OptimizePluginLoader.prototype.initialize = function (callback) {
        return __awaiter$2(this, void 0, void 0, function () {
            var functions, promises;
            return __generator$2(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        functions = this.find('initialize');
                        promises = functions.map(function (func) { return callback(func); });
                        return [4 /*yield*/, Promise.all(promises)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    OptimizePluginLoader.prototype.loadIntents = function () {
        return __awaiter$2(this, void 0, void 0, function () {
            var functions, promises, resolved, response;
            return __generator$2(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        functions = this.find('onLoadIntents');
                        promises = functions.map(function (func) { return func(); });
                        return [4 /*yield*/, Promise.all(promises)];
                    case 1:
                        resolved = _a.sent();
                        response = [];
                        resolved.forEach(function (value) {
                            if (value) {
                                response.push.apply(response, value);
                            }
                        });
                        return [2 /*return*/, response];
                }
            });
        });
    };
    OptimizePluginLoader.prototype.scoringChange = function (callback) {
        var functions = this.find('onScoringChange');
        functions.forEach(function (func) { return callback(func); });
    };
    OptimizePluginLoader.prototype.mapPlugins = function (plugins) {
        var map = {};
        plugins.forEach(function (plugin) {
            Object.keys(plugin).forEach(function (name) {
                map[name] = map[name] || [];
                map[name].push({
                    plugin: plugin,
                    function: plugin[name],
                });
            });
        });
        return map;
    };
    OptimizePluginLoader.prototype.findPlugins = function (type) {
        var mapped = this._pluginMap[type] || [];
        return mapped.map(function (p) { return p.plugin; });
    };
    OptimizePluginLoader.prototype.find = function (type) {
        var mapped = this._pluginMap[type] || [];
        return mapped.map(function (p) { return p.function; });
    };
    OptimizePluginLoader.prototype.findSingle = function (type) {
        var _a = this.find(type), firstPlugin = _a[0], rest = _a.slice(1);
        if (rest && rest.length) {
            console.log("Multiple plugins registered for " + type);
        }
        return firstPlugin;
    };
    return OptimizePluginLoader;
}());

// Unique ID creation requires a high quality random # generator. In the browser we therefore
// require the crypto API and do not support built-in fallback to lower quality random number
// generators (like Math.random()).
var getRandomValues;
var rnds8 = new Uint8Array(16);
function rng() {
  // lazy load so that environments that need to polyfill have a chance to do so
  if (!getRandomValues) {
    // getRandomValues needs to be invoked in a context where "this" is a Crypto implementation. Also,
    // find the complete implementation of crypto (msCrypto) on IE11.
    getRandomValues = typeof crypto !== 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || typeof msCrypto !== 'undefined' && typeof msCrypto.getRandomValues === 'function' && msCrypto.getRandomValues.bind(msCrypto);

    if (!getRandomValues) {
      throw new Error('crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported');
    }
  }

  return getRandomValues(rnds8);
}

var REGEX = /^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i;

function validate(uuid) {
  return typeof uuid === 'string' && REGEX.test(uuid);
}

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */

var byteToHex = [];

for (var i = 0; i < 256; ++i) {
  byteToHex.push((i + 0x100).toString(16).substr(1));
}

function stringify(arr) {
  var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  // Note: Be careful editing this code!  It's been tuned for performance
  // and works in ways you may not expect. See https://github.com/uuidjs/uuid/pull/434
  var uuid = (byteToHex[arr[offset + 0]] + byteToHex[arr[offset + 1]] + byteToHex[arr[offset + 2]] + byteToHex[arr[offset + 3]] + '-' + byteToHex[arr[offset + 4]] + byteToHex[arr[offset + 5]] + '-' + byteToHex[arr[offset + 6]] + byteToHex[arr[offset + 7]] + '-' + byteToHex[arr[offset + 8]] + byteToHex[arr[offset + 9]] + '-' + byteToHex[arr[offset + 10]] + byteToHex[arr[offset + 11]] + byteToHex[arr[offset + 12]] + byteToHex[arr[offset + 13]] + byteToHex[arr[offset + 14]] + byteToHex[arr[offset + 15]]).toLowerCase(); // Consistency check for valid UUID.  If this throws, it's likely due to one
  // of the following:
  // - One or more input array values don't map to a hex octet (leading to
  // "undefined" in the uuid)
  // - Invalid input values for the RFC `version` or `variant` fields

  if (!validate(uuid)) {
    throw TypeError('Stringified UUID is invalid');
  }

  return uuid;
}

function v4(options, buf, offset) {
  options = options || {};
  var rnds = options.random || (options.rng || rng)(); // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`

  rnds[6] = rnds[6] & 0x0f | 0x40;
  rnds[8] = rnds[8] & 0x3f | 0x80; // Copy bytes to buffer, if provided

  if (buf) {
    offset = offset || 0;

    for (var i = 0; i < 16; ++i) {
      buf[offset + i] = rnds[i];
    }

    return buf;
  }

  return stringify(rnds);
}

var __awaiter$3 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$3 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var scopeStrategy = function (options) {
    var defaultCreateScope = function () {
        var scope = {
            id: v4(),
            created: new Date().valueOf(),
            state: {},
            type: options.type,
            random: Math.random(),
        };
        return scope;
    };
    var isScopeExpired = function (scopeData) {
        if (typeof options.expirationMinutes === 'undefined') {
            return false;
        }
        var expirationMilliseconds = options.expirationMinutes * 60 * 1000;
        var currentTimestamp = new Date().valueOf();
        var timestamp = scopeData.updated || scopeData.created;
        var difference = currentTimestamp - timestamp;
        var isExpired = difference >= expirationMilliseconds;
        return isExpired;
    };
    var scopeStorage = options.scopeStorage, type = options.type;
    var scopeData;
    return {
        type: type,
        initialize: function (_a) {
            var log = _a.log;
            return __awaiter$3(void 0, void 0, void 0, function () {
                var updatedTimestamp, createScope;
                return __generator$3(this, function (_b) {
                    switch (_b.label) {
                        case 0: return [4 /*yield*/, scopeStorage.getScope(type)];
                        case 1:
                            scopeData = _b.sent();
                            if (!(scopeData && isScopeExpired(scopeData))) return [3 /*break*/, 4];
                            log({ level: 'info', message: "Scope " + type + " is expired" });
                            if (!options.scopeExpired) return [3 /*break*/, 3];
                            return [4 /*yield*/, options.scopeExpired(scopeData.id)];
                        case 2:
                            _b.sent();
                            _b.label = 3;
                        case 3:
                            scopeData = undefined;
                            return [3 /*break*/, 5];
                        case 4:
                            if (scopeData) {
                                updatedTimestamp = new Date().valueOf();
                                log({
                                    level: 'verbose',
                                    message: "Current scope valid, refreshing updated timestamp",
                                    type: type,
                                    previous: scopeData.updated,
                                    updated: updatedTimestamp,
                                });
                                scopeData.updated = updatedTimestamp;
                            }
                            _b.label = 5;
                        case 5:
                            if (!scopeData) {
                                log({ level: 'verbose', message: "Scope was not found, creating scope " + type });
                                createScope = (options === null || options === void 0 ? void 0 : options.createScope) || defaultCreateScope;
                                scopeData = createScope();
                            }
                            return [4 /*yield*/, scopeStorage.setScope(scopeData)];
                        case 6:
                            _b.sent();
                            return [2 /*return*/];
                    }
                });
            });
        },
        get: function () {
            return scopeData;
        },
        save: function () { return __awaiter$3(void 0, void 0, void 0, function () {
            return __generator$3(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!scopeData) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, scopeStorage.setScope(scopeData)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); },
        getExpiration: function () {
            return options.expirationMinutes;
        },
    };
};

var createVisitScopeStrategy = function (storage, options) {
    return scopeStrategy({
        type: Scope.Visit,
        scopeStorage: storage,
        expirationMinutes: (options === null || options === void 0 ? void 0 : options.expirationMinutes) || 20,
    });
};

var createVisitorScopeStrategy = function (storage) {
    return scopeStrategy({
        type: Scope.Visitor,
        scopeStorage: storage,
    });
};

var inMemoryScopeStorage = function () {
    var scopes = {};
    return {
        delete: function () {
            Object.keys(scopes).forEach(function (key) {
                delete scopes[key];
            });
            return Promise.resolve();
        },
        getScope: function (type) {
            return Promise.resolve(scopes[type]);
        },
        setScope: function (scope) {
            scopes[scope.type] = scope;
            return Promise.resolve();
        },
        isSupported: function () {
            return true;
        },
    };
};

var __awaiter$4 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$4 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var inMemoryActivityStorage = function () {
    var allActivity = [];
    return {
        delete: function () {
            if (allActivity.length) {
                allActivity.splice(0, allActivity.length);
            }
            return Promise.resolve();
        },
        list: function (type) { return __awaiter$4(void 0, void 0, void 0, function () {
            var filtered;
            return __generator$4(this, function (_a) {
                filtered = allActivity.filter(function (a) { return a.type === type; }).map(function (a) { return a; });
                return [2 /*return*/, Promise.resolve(filtered)];
            });
        }); },
        save: function (activity) {
            allActivity.push(activity);
            return Promise.resolve();
        },
        isSupported: function () {
            return true;
        },
    };
};

var inMemoryScoringStorage = function () {
    var scoring = {};
    return {
        getScoring: function (scope) {
            return Promise.resolve(scoring[scope]);
        },
        setScoring: function (value, scope) {
            scoring[scope] = value;
            return Promise.resolve();
        },
        delete: function () {
            scoring = {};
            return Promise.resolve();
        },
        isSupported: function () {
            return true;
        },
    };
};

var createPercentageTestStrategy = function (options) {
    var siteSampleSize = (options === null || options === void 0 ? void 0 : options.sampleSize) || 0;
    return {
        isIncludedInTest: function (scope, sampleSize) {
            var resolvedSampleSize = typeof sampleSize !== 'undefined' ? sampleSize : siteSampleSize;
            var include = scope ? resolvedSampleSize > scope.random : false;
            return include;
        },
    };
};

var addBrowserExtension = function (options) {
    var _a = options || {}, disabled = _a.disabled, _b = _a.logLevelThreshold, logLevelThreshold = _b === void 0 ? 'verbose' : _b;
    return {
        name: 'browser-extension',
        onLogMessage: function (options) {
            if (!disabled &&
                typeof window !== 'undefined' &&
                typeof window.__UNIFORM_APP_LISTENER__ === 'function') {
                var level = options.level;
                var shouldOutput = isLogLevelUnderThreshold(logLevelThreshold, level);
                if (!shouldOutput) {
                    return;
                }
                window.__UNIFORM_APP_LISTENER__({
                    type: 'log',
                    message: JSON.stringify(options),
                });
            }
        },
    };
};

const instanceOfAny = (object, constructors) => constructors.some((c) => object instanceof c);

let idbProxyableTypes;
let cursorAdvanceMethods;
// This is a function to prevent it throwing up in node environments.
function getIdbProxyableTypes() {
    return (idbProxyableTypes ||
        (idbProxyableTypes = [
            IDBDatabase,
            IDBObjectStore,
            IDBIndex,
            IDBCursor,
            IDBTransaction,
        ]));
}
// This is a function to prevent it throwing up in node environments.
function getCursorAdvanceMethods() {
    return (cursorAdvanceMethods ||
        (cursorAdvanceMethods = [
            IDBCursor.prototype.advance,
            IDBCursor.prototype.continue,
            IDBCursor.prototype.continuePrimaryKey,
        ]));
}
const cursorRequestMap = new WeakMap();
const transactionDoneMap = new WeakMap();
const transactionStoreNamesMap = new WeakMap();
const transformCache = new WeakMap();
const reverseTransformCache = new WeakMap();
function promisifyRequest(request) {
    const promise = new Promise((resolve, reject) => {
        const unlisten = () => {
            request.removeEventListener('success', success);
            request.removeEventListener('error', error);
        };
        const success = () => {
            resolve(wrap(request.result));
            unlisten();
        };
        const error = () => {
            reject(request.error);
            unlisten();
        };
        request.addEventListener('success', success);
        request.addEventListener('error', error);
    });
    promise
        .then((value) => {
        // Since cursoring reuses the IDBRequest (*sigh*), we cache it for later retrieval
        // (see wrapFunction).
        if (value instanceof IDBCursor) {
            cursorRequestMap.set(value, request);
        }
        // Catching to avoid "Uncaught Promise exceptions"
    })
        .catch(() => { });
    // This mapping exists in reverseTransformCache but doesn't doesn't exist in transformCache. This
    // is because we create many promises from a single IDBRequest.
    reverseTransformCache.set(promise, request);
    return promise;
}
function cacheDonePromiseForTransaction(tx) {
    // Early bail if we've already created a done promise for this transaction.
    if (transactionDoneMap.has(tx))
        return;
    const done = new Promise((resolve, reject) => {
        const unlisten = () => {
            tx.removeEventListener('complete', complete);
            tx.removeEventListener('error', error);
            tx.removeEventListener('abort', error);
        };
        const complete = () => {
            resolve();
            unlisten();
        };
        const error = () => {
            reject(tx.error || new DOMException('AbortError', 'AbortError'));
            unlisten();
        };
        tx.addEventListener('complete', complete);
        tx.addEventListener('error', error);
        tx.addEventListener('abort', error);
    });
    // Cache it for later retrieval.
    transactionDoneMap.set(tx, done);
}
let idbProxyTraps = {
    get(target, prop, receiver) {
        if (target instanceof IDBTransaction) {
            // Special handling for transaction.done.
            if (prop === 'done')
                return transactionDoneMap.get(target);
            // Polyfill for objectStoreNames because of Edge.
            if (prop === 'objectStoreNames') {
                return target.objectStoreNames || transactionStoreNamesMap.get(target);
            }
            // Make tx.store return the only store in the transaction, or undefined if there are many.
            if (prop === 'store') {
                return receiver.objectStoreNames[1]
                    ? undefined
                    : receiver.objectStore(receiver.objectStoreNames[0]);
            }
        }
        // Else transform whatever we get back.
        return wrap(target[prop]);
    },
    set(target, prop, value) {
        target[prop] = value;
        return true;
    },
    has(target, prop) {
        if (target instanceof IDBTransaction &&
            (prop === 'done' || prop === 'store')) {
            return true;
        }
        return prop in target;
    },
};
function replaceTraps(callback) {
    idbProxyTraps = callback(idbProxyTraps);
}
function wrapFunction(func) {
    // Due to expected object equality (which is enforced by the caching in `wrap`), we
    // only create one new func per func.
    // Edge doesn't support objectStoreNames (booo), so we polyfill it here.
    if (func === IDBDatabase.prototype.transaction &&
        !('objectStoreNames' in IDBTransaction.prototype)) {
        return function (storeNames, ...args) {
            const tx = func.call(unwrap(this), storeNames, ...args);
            transactionStoreNamesMap.set(tx, storeNames.sort ? storeNames.sort() : [storeNames]);
            return wrap(tx);
        };
    }
    // Cursor methods are special, as the behaviour is a little more different to standard IDB. In
    // IDB, you advance the cursor and wait for a new 'success' on the IDBRequest that gave you the
    // cursor. It's kinda like a promise that can resolve with many values. That doesn't make sense
    // with real promises, so each advance methods returns a new promise for the cursor object, or
    // undefined if the end of the cursor has been reached.
    if (getCursorAdvanceMethods().includes(func)) {
        return function (...args) {
            // Calling the original function with the proxy as 'this' causes ILLEGAL INVOCATION, so we use
            // the original object.
            func.apply(unwrap(this), args);
            return wrap(cursorRequestMap.get(this));
        };
    }
    return function (...args) {
        // Calling the original function with the proxy as 'this' causes ILLEGAL INVOCATION, so we use
        // the original object.
        return wrap(func.apply(unwrap(this), args));
    };
}
function transformCachableValue(value) {
    if (typeof value === 'function')
        return wrapFunction(value);
    // This doesn't return, it just creates a 'done' promise for the transaction,
    // which is later returned for transaction.done (see idbObjectHandler).
    if (value instanceof IDBTransaction)
        cacheDonePromiseForTransaction(value);
    if (instanceOfAny(value, getIdbProxyableTypes()))
        return new Proxy(value, idbProxyTraps);
    // Return the same value back if we're not going to transform it.
    return value;
}
function wrap(value) {
    // We sometimes generate multiple promises from a single IDBRequest (eg when cursoring), because
    // IDB is weird and a single IDBRequest can yield many responses, so these can't be cached.
    if (value instanceof IDBRequest)
        return promisifyRequest(value);
    // If we've already transformed this value before, reuse the transformed value.
    // This is faster, but it also provides object equality.
    if (transformCache.has(value))
        return transformCache.get(value);
    const newValue = transformCachableValue(value);
    // Not all types are transformed.
    // These may be primitive types, so they can't be WeakMap keys.
    if (newValue !== value) {
        transformCache.set(value, newValue);
        reverseTransformCache.set(newValue, value);
    }
    return newValue;
}
const unwrap = (value) => reverseTransformCache.get(value);

/**
 * Open a database.
 *
 * @param name Name of the database.
 * @param version Schema version.
 * @param callbacks Additional callbacks.
 */
function openDB(name, version, { blocked, upgrade, blocking, terminated } = {}) {
    const request = indexedDB.open(name, version);
    const openPromise = wrap(request);
    if (upgrade) {
        request.addEventListener('upgradeneeded', (event) => {
            upgrade(wrap(request.result), event.oldVersion, event.newVersion, wrap(request.transaction));
        });
    }
    if (blocked)
        request.addEventListener('blocked', () => blocked());
    openPromise
        .then((db) => {
        if (terminated)
            db.addEventListener('close', () => terminated());
        if (blocking)
            db.addEventListener('versionchange', () => blocking());
    })
        .catch(() => { });
    return openPromise;
}
/**
 * Delete a database.
 *
 * @param name Name of the database.
 */
function deleteDB(name, { blocked } = {}) {
    const request = indexedDB.deleteDatabase(name);
    if (blocked)
        request.addEventListener('blocked', () => blocked());
    return wrap(request).then(() => undefined);
}

const readMethods = ['get', 'getKey', 'getAll', 'getAllKeys', 'count'];
const writeMethods = ['put', 'add', 'delete', 'clear'];
const cachedMethods = new Map();
function getMethod(target, prop) {
    if (!(target instanceof IDBDatabase &&
        !(prop in target) &&
        typeof prop === 'string')) {
        return;
    }
    if (cachedMethods.get(prop))
        return cachedMethods.get(prop);
    const targetFuncName = prop.replace(/FromIndex$/, '');
    const useIndex = prop !== targetFuncName;
    const isWrite = writeMethods.includes(targetFuncName);
    if (
    // Bail if the target doesn't exist on the target. Eg, getAll isn't in Edge.
    !(targetFuncName in (useIndex ? IDBIndex : IDBObjectStore).prototype) ||
        !(isWrite || readMethods.includes(targetFuncName))) {
        return;
    }
    const method = async function (storeName, ...args) {
        // isWrite ? 'readwrite' : undefined gzipps better, but fails in Edge :(
        const tx = this.transaction(storeName, isWrite ? 'readwrite' : 'readonly');
        let target = tx.store;
        if (useIndex)
            target = target.index(args.shift());
        const returnVal = await target[targetFuncName](...args);
        if (isWrite)
            await tx.done;
        return returnVal;
    };
    cachedMethods.set(prop, method);
    return method;
}
replaceTraps((oldTraps) => ({
    ...oldTraps,
    get: (target, prop, receiver) => getMethod(target, prop) || oldTraps.get(target, prop, receiver),
    has: (target, prop) => !!getMethod(target, prop) || oldTraps.has(target, prop),
}));

var isIndexedDbSupported = function () {
    if (typeof window === 'undefined') {
        return false;
    }
    var windowAny = window;
    var indexeddb = window.indexedDB || windowAny.mozIndexedDB || windowAny.webkitIndexedDB || windowAny.msIndexedDB;
    return typeof indexeddb !== 'undefined';
};

var __assign$3 = (undefined && undefined.__assign) || function () {
    __assign$3 = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign$3.apply(this, arguments);
};
var __awaiter$5 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$5 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var databaseCallbacks = {
    upgrade: function (db) {
        var store = db.createObjectStore('activities', {
            keyPath: 'id',
            autoIncrement: true,
        });
        store.createIndex('by-visit', 'visitId');
        store.createIndex('by-path', 'path');
        store.createIndex('by-type', 'type');
    },
};
var databaseName = 'optimize-activities';
var openDatabase = function () { return __awaiter$5(void 0, void 0, void 0, function () {
    return __generator$5(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, openDB(databaseName, 1, databaseCallbacks)];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
var indexedDbActivityStorage = function () {
    return {
        save: function (activity) { return __awaiter$5(void 0, void 0, void 0, function () {
            var db;
            return __generator$5(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, openDatabase()];
                    case 1:
                        db = _a.sent();
                        return [4 /*yield*/, db.put('activities', __assign$3(__assign$3({}, activity), { updated: new Date().valueOf() }))];
                    case 2:
                        _a.sent();
                        db.close();
                        return [2 /*return*/];
                }
            });
        }); },
        list: function (type) { return __awaiter$5(void 0, void 0, void 0, function () {
            var db, activities;
            return __generator$5(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, openDatabase()];
                    case 1:
                        db = _a.sent();
                        return [4 /*yield*/, db.getAllFromIndex('activities', 'by-type', type)];
                    case 2:
                        activities = _a.sent();
                        db.close();
                        return [2 /*return*/, activities];
                }
            });
        }); },
        delete: function () { return __awaiter$5(void 0, void 0, void 0, function () {
            return __generator$5(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, deleteDB(databaseName)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); },
        isSupported: function () {
            var supported = isIndexedDbSupported();
            return supported;
        },
    };
};

var __awaiter$6 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$6 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var databaseCallbacks$1 = {
    upgrade: function (db) {
        var store = db.createObjectStore('scopes', {
            keyPath: 'id',
        });
        store.createIndex('by-type-date', ['type', 'created']);
    },
};
var databaseName$1 = 'optimize-scopes';
var openDatabase$1 = function () { return __awaiter$6(void 0, void 0, void 0, function () {
    return __generator$6(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, openDB(databaseName$1, 1, databaseCallbacks$1)];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
var indexedDbScopeStorage = function () {
    return {
        getScope: function (type) { return __awaiter$6(void 0, void 0, void 0, function () {
            var db, scope, index, keyRange, cursor;
            return __generator$6(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, openDatabase$1()];
                    case 1:
                        db = _a.sent();
                        index = db.transaction('scopes', 'readonly').store.index('by-type-date');
                        keyRange = IDBKeyRange.bound([type, 0], [type, new Date().getTime()]);
                        return [4 /*yield*/, index.openCursor(keyRange, 'prev')];
                    case 2:
                        cursor = _a.sent();
                        if (cursor) {
                            scope = cursor.value;
                        }
                        db.close();
                        return [2 /*return*/, scope];
                }
            });
        }); },
        setScope: function (scope) { return __awaiter$6(void 0, void 0, void 0, function () {
            var db;
            return __generator$6(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, openDatabase$1()];
                    case 1:
                        db = _a.sent();
                        return [4 /*yield*/, db.put('scopes', scope)];
                    case 2:
                        _a.sent();
                        db.close();
                        return [2 /*return*/];
                }
            });
        }); },
        delete: function () { return __awaiter$6(void 0, void 0, void 0, function () {
            return __generator$6(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, deleteDB(databaseName$1)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); },
        isSupported: function () {
            var supported = isIndexedDbSupported();
            return supported;
        },
    };
};

var __awaiter$7 = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator$7 = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var databaseName$2 = 'optimize-scoring';
var databaseCallbacks$2 = {
    upgrade: function (db) {
        db.createObjectStore('scoring');
    },
};
var openDatabase$2 = function () { return __awaiter$7(void 0, void 0, void 0, function () {
    return __generator$7(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, openDB(databaseName$2, 1, databaseCallbacks$2)];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
var indexedDbScoringStorage = function () {
    var scoringKey = 'scoring';
    return {
        getScoring: function (scope) { return __awaiter$7(void 0, void 0, void 0, function () {
            var db, scoring;
            return __generator$7(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, openDatabase$2()];
                    case 1:
                        db = _a.sent();
                        return [4 /*yield*/, db.get("scoring", scoringKey + ":" + scope)];
                    case 2:
                        scoring = _a.sent();
                        db.close();
                        return [2 /*return*/, scoring];
                }
            });
        }); },
        setScoring: function (value, scope) { return __awaiter$7(void 0, void 0, void 0, function () {
            var db;
            return __generator$7(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, openDatabase$2()];
                    case 1:
                        db = _a.sent();
                        return [4 /*yield*/, db.put('scoring', value, scoringKey + ":" + scope)];
                    case 2:
                        _a.sent();
                        db.close();
                        return [2 /*return*/];
                }
            });
        }); },
        delete: function () { return __awaiter$7(void 0, void 0, void 0, function () {
            return __generator$7(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, deleteDB(databaseName$2)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); },
        isSupported: function () {
            var supported = isIndexedDbSupported();
            return supported;
        },
    };
};

var __assign$4 = (undefined && undefined.__assign) || function () {
    __assign$4 = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign$4.apply(this, arguments);
};
var __spreadArrays$2 = (undefined && undefined.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var createDefaultPlugins = function (_a) {
    var _b;
    var intents = _a.intents, customPlugins = _a.customPlugins, logLevelThreshold = _a.logLevelThreshold, browserExtensionDisabled = _a.browserExtensionDisabled;
    var plugins = __spreadArrays$2([
        addBrowserExtension({ disabled: browserExtensionDisabled }),
        addConsoleLoggerPlugin(logLevelThreshold),
        addLocalIntentManifestPlugin(intents !== null && intents !== void 0 ? intents : (typeof window !== 'undefined' ? (_b = window === null || window === void 0 ? void 0 : window.__uniformapp) === null || _b === void 0 ? void 0 : _b.intents : undefined)),
        // signal types
        addBehaviorSignalPlugin(),
        addCookieSignalPlugin(),
        addEventSignalPlugin(),
        addLandingPageSignalPlugin(),
        addPageViewCountSignalPlugin(),
        addPageVisitedSignalPlugin(),
        addQueryStringSignalPlugin()
    ], customPlugins);
    var loader = new OptimizePluginLoader(plugins);
    return loader;
};
var buildStorage = function (storage) {
    if (storage === void 0) { storage = {}; }
    var isServer = typeof window === 'undefined';
    var storageDefaults = {
        activities: isServer ? inMemoryActivityStorage() : indexedDbActivityStorage(),
        scopes: isServer ? inMemoryScopeStorage() : indexedDbScopeStorage(),
        scoring: isServer ? inMemoryScoringStorage() : indexedDbScoringStorage(),
    };
    return __assign$4(__assign$4({}, storageDefaults), storage);
};
var createDefaultTracker = function (options) {
    var _a, _b;
    var windowConfig = undefined;
    var storage = buildStorage(options === null || options === void 0 ? void 0 : options.storage);
    var defaultConfig = {
        plugins: createDefaultPlugins({
            intents: options === null || options === void 0 ? void 0 : options.intentManifest,
            customPlugins: (options === null || options === void 0 ? void 0 : options.addPlugins) || [],
            logLevelThreshold: options === null || options === void 0 ? void 0 : options.logLevelThreshold,
            browserExtensionDisabled: options === null || options === void 0 ? void 0 : options.browserExtensionDisabled,
        }),
        scopes: {
            visit: createVisitScopeStrategy(storage.scopes),
            visitor: createVisitorScopeStrategy(storage.scopes),
        },
        storage: storage,
        testing: (options === null || options === void 0 ? void 0 : options.testing) ? options.testing
            : {
                strategy: createPercentageTestStrategy({
                    sampleSize: (_b = (_a = options === null || options === void 0 ? void 0 : options.intentManifest) === null || _a === void 0 ? void 0 : _a.site) === null || _b === void 0 ? void 0 : _b.sampleSize,
                }),
            },
    };
    if (typeof window !== 'undefined') {
        windowConfig = window === null || window === void 0 ? void 0 : window.__uniformapp;
        if (windowConfig) {
            windowConfig = __assign$4(__assign$4({}, defaultConfig), windowConfig);
        }
    }
    var finalConfig = __assign$4(__assign$4({}, defaultConfig), windowConfig);
    return new OptimizeTracker(finalConfig);
};

export { createDefaultTracker };
