import { g as getPersonalizationIntents } from '../common/IntentTags-dc424c30.js';

var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
/** Normalizes `v1` and `v2` so they have congruent axes, and any missing axes are assigned 0 as a magnitude */
function normalizeCoordinates(v1, v2) {
    var result = {
        v1Prime: __assign({}, v1),
        v2Prime: __assign({}, v2),
    };
    var sharesAnyCoordinateLabel = false;
    Object.keys(v1).forEach(function (k) {
        if (typeof v2[k] === 'undefined') {
            result.v2Prime[k] = { str: 0 };
            return;
        }
        // we need to know if there is any overlap between the keys,
        // since we don't want to calculate a fake 'distance' from non-overlapping
        // coordinate systems (i.e. a,b,c and z,y,x dont share an axis)
        sharesAnyCoordinateLabel = true;
    });
    if (!sharesAnyCoordinateLabel) {
        return;
    }
    Object.keys(v2).forEach(function (k) {
        if (typeof v1[k] === 'undefined') {
            result.v1Prime[k] = { str: 0 };
        }
    });
    return result;
}
function euclideanDistance(v1, v2) {
    // Euclidean distance = sqrt(Σ (c2n-c1n)^2)
    var matches = [];
    var squaredCoordinateSummation = Object.keys(v1).reduce(function (currentSum, key) {
        var _a, _b;
        var v2Str = (_a = v2[key]) === null || _a === void 0 ? void 0 : _a.str;
        var v1Str = (_b = v1[key]) === null || _b === void 0 ? void 0 : _b.str;
        if (typeof v2Str !== 'undefined' && typeof v1Str !== 'undefined' && v2Str && v1Str) {
            matches.push(key);
        }
        return (currentSum += Math.pow(Number(v2Str !== null && v2Str !== void 0 ? v2Str : 0) - Number(v1Str !== null && v1Str !== void 0 ? v1Str : 0), 2));
    }, 0);
    return {
        distance: Math.sqrt(squaredCoordinateSummation),
        matches: matches,
    };
}
var calculateScore = function (_a) {
    var intentScores = _a.intentScores, intentTag = _a.intentTag;
    // no visitor intent score, or else this list item has no intent tag = default
    if (!intentScores || !intentTag) {
        return { isDefault: true };
    }
    var personalizationEnabledIntentTags = getPersonalizationIntents(intentTag);
    // intent tag data was present, but either no tags were added,
    // or no tags allowed for personalization use = default
    if (!personalizationEnabledIntentTags || Object.keys(personalizationEnabledIntentTags).length === 0) {
        return { isDefault: true };
    }
    var normalized = normalizeCoordinates(intentScores, personalizationEnabledIntentTags);
    // no intent scores you had matched tag intents on this item
    // it's personalized...just not for you
    if (typeof normalized === 'undefined') {
        return { score: Number.POSITIVE_INFINITY, isDefault: false };
    }
    var _b = euclideanDistance(normalized.v1Prime, normalized.v2Prime), distance = _b.distance, matches = _b.matches;
    return {
        score: distance,
        isDefault: false,
        intentTag: intentTag,
        matches: matches,
    };
};
var compareOverride = function (a, b) {
    var _a, _b, _c, _d, _e, _f;
    var aHasOverrides = Object.keys((_c = (_b = (_a = a.item) === null || _a === void 0 ? void 0 : _a.intentTag) === null || _b === void 0 ? void 0 : _b.intents) !== null && _c !== void 0 ? _c : {}).some(function (sig) { var _a, _b, _c; return ((_a = a.matches) === null || _a === void 0 ? void 0 : _a.includes(sig)) && ((_c = (_b = a.item) === null || _b === void 0 ? void 0 : _b.intentTag) === null || _c === void 0 ? void 0 : _c.intents[sig].override); });
    var bHasOverrides = Object.keys((_f = (_e = (_d = b.item) === null || _d === void 0 ? void 0 : _d.intentTag) === null || _e === void 0 ? void 0 : _e.intents) !== null && _f !== void 0 ? _f : {}).some(function (sig) { var _a, _b, _c; return ((_a = b.matches) === null || _a === void 0 ? void 0 : _a.includes(sig)) && ((_c = (_b = b.item) === null || _b === void 0 ? void 0 : _b.intentTag) === null || _c === void 0 ? void 0 : _c.intents[sig].override); });
    if (aHasOverrides && !bHasOverrides) {
        return -1;
    }
    if (!aHasOverrides && bHasOverrides) {
        return 1;
    }
    return 0;
};
var compareRelevancyAsc = function (a, b) {
    var _a, _b, _c, _d, _e, _f;
    // infinity === no matches, push to bottom
    if (b.relevancy === undefined && a.relevancy === Number.POSITIVE_INFINITY) {
        return 1;
    }
    if (((_a = a.matches) === null || _a === void 0 ? void 0 : _a.length) === ((_b = b.matches) === null || _b === void 0 ? void 0 : _b.length)) {
        return (a.relevancy || 0) - (b.relevancy || 0);
    }
    var aMatches = (_d = (_c = a.matches) === null || _c === void 0 ? void 0 : _c.length) !== null && _d !== void 0 ? _d : 0;
    var bMatches = (_f = (_e = b.matches) === null || _e === void 0 ? void 0 : _e.length) !== null && _f !== void 0 ? _f : 0;
    return bMatches - aMatches;
};
/**
 * Orders a list of objects that contain personalization information by the order of relevancy to a intent score.
 * The default ordering is by n-dimensional euclidean distance of the intent tag from the intent score.
 */
var personalizeList = function (_a) {
    var list = _a.list, intentScores = _a.intentScores, scorer = _a.scorer, fallbackSort = _a.fallbackSort, analyticsEmitter = _a.analyticsEmitter, isIncludedInTest = _a.isIncludedInTest, count = _a.count, personalizedOnly = _a.personalizedOnly;
    var wasAnythingScored = false;
    var scoreFunc = scorer !== null && scorer !== void 0 ? scorer : calculateScore;
    var allMatches = {};
    var scoredItems = list.map(function (listItem) {
        var _a = scoreFunc({ intentTag: listItem.intentTag, intentScores: intentScores }), score = _a.score, isDefault = _a.isDefault, matches = _a.matches;
        if (matches) {
            matches.forEach(function (key) {
                allMatches[key] = true;
            });
        }
        if (typeof score !== 'undefined' && score !== Number.POSITIVE_INFINITY) {
            wasAnythingScored = true;
        }
        var result = {
            item: listItem,
            matches: matches,
            relevancy: score,
            isDefault: isDefault,
        };
        return result;
    });
    if (wasAnythingScored && !isIncludedInTest) {
        // sort by relevancy ASC (lower relevancy numbers are more relevant since relevancy is a distance)
        scoredItems.sort(function (a, b) {
            var overrideResult = compareOverride(a, b);
            if (overrideResult !== 0) {
                return overrideResult;
            }
            var relevancyResult = compareRelevancyAsc(a, b);
            if (relevancyResult === 0 && fallbackSort) {
                return fallbackSort(a.item, b.item);
            }
            return relevancyResult;
        });
    }
    else {
        // If nothing was scored, sort default items first.
        scoredItems.sort(function (a, b) {
            if (a.isDefault === b.isDefault) {
                return fallbackSort ? fallbackSort(a.item, b.item) : 0;
            }
            return Number(b.isDefault) > Number(a.isDefault) ? 1 : -1;
        });
    }
    var matches = Object.keys(allMatches);
    analyticsEmitter === null || analyticsEmitter === void 0 ? void 0 : analyticsEmitter.personalization({
        matches: matches,
        isIncludedInTest: isIncludedInTest,
    });
    var result = scoredItems;
    // if `personalizedOnly` is true, filter out non-personalized items
    if (personalizedOnly) {
        result = result.filter(function (v) { return !v.isDefault && v.relevancy !== Infinity; });
    }
    // take first x results if `count` is specified
    if (typeof count === 'number' && count > 0) {
        result = result.slice(0, count);
    }
    return {
        result: result,
        personalized: !isIncludedInTest && wasAnythingScored,
        intentScores: intentScores,
    };
};

export { personalizeList };
