import {localTracker} from "./localTracker.js";
import {getPersonalizedContent} from "./getPersonalizedContent.js";
export async function initTracker() {
  localTracker.addScoringChangeListener(onScoringChanged);
  await localTracker.initialize();
  await localTracker.reevaluateSignals();
}
initTracker();
function onScoringChanged(intentScores) {  
  console.log('personalize onScoringChanged>>>',JSON.stringify(intentScores, null, 2));
  //flag in local storage
  localStorage.setItem('intent',intentScores ? getPersonalizedContent(intentScores) : "");
  
}

document.addEventListener( 'ROUTE_CHANGED', () => {
  console.log('Route Changed >>>>>');
  initTracker();
} );

