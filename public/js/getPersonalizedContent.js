import {IntentTagStrength} from "./web_modules/@uniformdev/optimize-common.js";
import {personalizeList} from "./web_modules/@uniformdev/optimize-tracker.js";
const variations = [
  {
    content: "",
    intentTag: void 0
  },  
  {
    content: "Watches",
    intentTag: {
      intents: {
        "extra-event-intent": {
          str: IntentTagStrength.Normal
        }
      }
    }
  }
  
];
export function getPersonalizedContent(intentScores) {  
  const personalizedList = personalizeList({
    intentScores,
    list: variations
  });
  console.log(personalizedList.result[0].item.content);
  
  return personalizedList.result[0].item.content;
}
