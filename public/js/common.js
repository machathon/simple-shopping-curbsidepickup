window.dataLayer = window.dataLayer || [];
$(document).ready(function () {
    simpleCart({
        cartStyle: "div",
        cartColumns: [
            { view: "image", attr: "thumb", label: false },
            { attr: "name", label: "Name" },
            { attr: "price", label: "Price", view: "currency" },
            { view: "decrement", label: false, text: " - " },
            { attr: "quantity", label: "Qty" },
            { view: "increment", label: false, text: " +" },
            { view: "remove", text: "Remove", label: false }
        ]
    });
});

$(document).on('click', '.input-number-increment' , function(e) {
    var $input = $(this).parents('.input-number-group').find('.input-number');
    var val = parseInt($input.val(), 10);
    $input.val(val + 1);
});

$(document).on('click', '.input-number-decrement' , function(e) {
    var $input = $(this).parents('.input-number-group').find('.input-number');
    var val = parseInt($input.val(), 10);
    if (val != 0) {
        $input.val(val - 1);
    }
})

$(document).on('click', 'a.routerLink' , function(e) {
        routeURL(e);
        fireRouteChangedEvent();
        initBehavior(window.location.pathname);  
});

function fireRouteChangedEvent() {
    var routeChangeEvent = new CustomEvent("ROUTE_CHANGED", {});
    document.dispatchEvent(routeChangeEvent);
}

function routeURL(e){
        console.log('the routerlink has been clicked',e, e.target);
        let targetAction = e.target;
        let targetURL;
        if (targetAction instanceof HTMLAnchorElement)
        {
            console.log('el.getAttribute',targetAction.getAttribute('href'));
            targetURL = targetAction.getAttribute('href');
        }else{
            targetURL = targetAction.parentNode.getAttribute('href')
        }
        console.log('the targetURL clicked',targetURL);
        if(targetURL && shouldChangeRoute(targetURL)){
            loadDataAndChangeRoute(targetURL, e);
        }
}

function loadDataAndChangeRoute(targetURL, e) {
    loadServerContent(targetURL);
    console.log('targetURL>>>>>', targetURL);

        $( "div.navbar-nav a" ).each(function( index ) {
            if(targetURL == ($( this ).attr('href'))){
                $( this ).addClass('active');
            }else{
                $( this ).removeClass('active');
            }
            console.log( 'index',this + ": " + $( this ).text(), $( this ).attr('href') );
            
          });
        
          $( "div.navbar-collapse" ).removeClass('show');

         console.log('this is a category URL')
    if(e){
        e.preventDefault();
    }
    history.pushState({}, '', targetURL);
    $('div#MainContent').focus();
}

function loadServerContent( targetURL ){

    $.get(targetURL, function (response) {
        var source = $("<div>" + response + "</div>");
        console.log('Source >>>>', source)
        console.log('source.find(div#MainContent).html() >>>>', source.find('div#MainContent'));
        $('div#MainContent').html(source.find('div#MainContent'));
        });
}    

$(document).on('submit', '#searchForm' , function(e) { 
    // this line prevents the normal behaviour of the form (ie: reloading the page with form data sent as POST data)
   
    // now we can check the form data
    var searchtermValue = this.searchterm.value;
    let targetURL;
    if (searchtermValue !== null && searchtermValue !== '') {
        targetURL = "/search/" + searchtermValue;
    }
    performSearch(targetURL, e);
    this.searchterm.value = '';
});

function performSearch(targetURL, e) {

    if (shouldChangeRoute( targetURL )) {
        console.log('performSearch shouldChangeRoute>>>>>', targetURL);
        loadDataAndChangeRoute(targetURL, e);
        fireRouteChangedEvent();
        initBehavior(window.location.pathname);
    } else {
        console.log('performSearch window.location.href >>>>>', targetURL);
        if(e){
           e.preventDefault(); 
        }
        window.location.href = targetURL;
    }
}

function runSpeechRecognition() {
		        
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();

    // This runs when the speech recognition service starts
    recognition.onstart = function() {                    
    };
    
    recognition.onspeechend = function() {                    
        recognition.stop();
    }
  
    // This runs when the speech recognition service returns result
    recognition.onresult = function(event) {
        var transcript = event.results[0][0].transcript;
       document.getElementById("searchBox").value = transcript; 
       if(transcript !== null && transcript !== '') {
         let targetURL = "/search/"+transcript;
         performSearch(targetURL,'');
        }                 
    };
  
     // start recognition
     recognition.start();
}

function shouldChangeRoute( targetURL ) {
    return ($('div#MainContent').length != 0 && (targetURL.startsWith('/category') || targetURL.startsWith('/search')));
}

//Ideally to be Driven from CMS- Use Static Content for now!
function getWatchesContent(){
    var personalisedWatchesContent= '<h3>Looks like you are looking for Watches. Checkout the New Smart Watch</h3>+<img  src="https://eu-images.contentstack.com/v3/assets/blt385828df08222b81/blta9146172c0e1e789/60115399b2ffda0ce5472900/smart_watch1.jpg?width=396&height=348" </img>+<a href="/product/793fcfdb-9c26-42b0-b551-8cd33b822a12" class="btn main-banner-btn">Shop Now</a>'; 
    return personalisedWatchesContent;

}

function initBehavior(categoryName) {
    //Track Signal by for each Category Page Landing for the user
      
    document.getElementById("personalised-banner")?.addEventListener("load", async () => {
      await localTracker.addEvent({
        label: "fire",
        category: "anything",
        value: "signal only cares about label=fire"
      });
    });
}